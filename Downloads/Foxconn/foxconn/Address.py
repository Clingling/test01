# -*- coding:UTF-8 -*-

from selenium import webdriver

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
import Login
import unittest
import time
import data
import re

driver = data.return_driver()

class Address(unittest.TestCase):
    def setUp(self):
        pass

    def test_Address_manage(self):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        #点击通讯录管理
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.LINK_TEXT, "通讯录管理"))).click()
        print "Enter the Address management interface"


   #添加目录
    def add_Unit(self,Unit_name,):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "rightFrame")))
        #点击添加目录
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'addUnit'))).click()
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))

        time.sleep(3)
        #输入目录名
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).send_keys(Unit_name)
        #点击保存
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'savebtn'))).click()




    #添加目录成功检查
    def test_add_Unit_success(self):
        self.add_Unit(data.Unit_name[0])
        #检查保存成功提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.address_info[0], alert)
        time.sleep(10)
        # 关闭提示框
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()


  #添目录名称重复检查
    def test_add_Unit_repeat(self):
        self.add_Unit(data.Unit_name[3])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.address_info[2], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        #点击取消
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()


        # 添目录称为空检查
    def test_add_Unit_empty(self):
        self.add_Unit(data.Unit_name[2])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.address_info[1], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()

    #修改目录名检查
    def edit_Unit(self, Unit_name):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        #切换左边的iframe
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "leftFrame")))
        time.sleep(1)
        #点击大类名称
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/div/ul/li/ul/li[1]/div/span[4]/span'))).click()
        #切换右边的iframe
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "rightFrame")))
      # 点击修改

        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'modifyUnit'))).click()
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        time.sleep(3)
        # 清除大类名称
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).clear()
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).send_keys(Unit_name)
        # 点击保存
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'savebtn'))).click()


    #修改目录成功检查
    def test_edit_Unit_success(self):
        self.edit_Unit(data.Unit_name[1])
        # 检查保存成功提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.address_info[3], alert)
        time.sleep(10)
        # 关闭提示框
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()

        # 修改目录名称重复检查
    def test_edit_Unit_repeat(self):
         self.edit_Unit(data.Unit_name[3])
         alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
         print alert
         self.assertIn(data.address_info[2], alert)
         # 关闭提示框
         time.sleep(10)
         # 点击确定
         WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
         # 点击取消
         time.sleep(1)
         WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()

    # 修改目录名称为空检查
    def test_edit_Unit_empty(self):
         self.edit_Unit(data.Unit_name[2])
         alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
         print alert
         self.assertIn(data.address_info[1], alert)
         # 关闭提示框
         time.sleep(10)
         # 点击确定
         WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
         # 点击取消
         time.sleep(1)
         WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()




     #新增联系人
    def add_Person(self,Person_name, phone,emp,job,job_number,email,remark):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        # 切换左边的iframe
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "leftFrame")))
        time.sleep(1)
        # 点击大类名称
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/div/ul/li/ul/li[1]/div/span[4]/span'))).click()

        # 切换右边的iframe
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "rightFrame")))

        # 点击新增联系人
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'addPerson'))).click()
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).send_keys(Person_name)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'phone'))).send_keys(phone)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'company'))).send_keys(emp)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'job'))).send_keys(job)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'address'))).send_keys(job_number)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'email'))).send_keys(email)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'remark'))).send_keys(remark)


        # 点击保存
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'savebtn'))).click()



        # 添加联系人成功检查
    def test_add_Person_success(self):
        self.add_Person(data.Person_name[0] , data.phone[0], data.emp[0],data.job[0],data.job_number[0],data.email[0],data.remark[0])
        # 检查保存成功提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.address_info[0], alert)
        time.sleep(10)
        # 关闭提示框
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()


        # 添联系人姓名为空检查
    def test_add_Person_name_empty(self):
        self.add_Person(data.Person_name[2], data.phone[0], data.emp[0], data.job[0], data.job_number[0], data.email[0],data.remark[0])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.address_info[4], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()



        # 添联系人电话为空检查
    def test_add_phone_empty(self):
        self.add_Person(data.Person_name[0], data.phone[2], data.emp[0], data.job[0], data.job_number[0], data.email[0],data.remark[0])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.address_info[6], alert)
         # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()



        # 添联系人电话格式错误检查
    def test_add_phone_format(self):
        self.add_Person(data.Person_name[0], data.phone[1], data.emp[0], data.job[0], data.job_number[0], data.email[0],data.remark[0])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.address_info[5], alert)
         # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()


        # 添联系人邮箱格式错误检查
    def test_add_email_format(self):
        self.add_Person(data.Person_name[0], data.phone[0], data.emp[0], data.job[0], data.job_number[0], data.email[1],data.remark[0])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.address_info[7], alert)
         # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()



    def edit_Person(self,Person_name, phone,emp,job,job_number,email,remark):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        # 切换左边的iframe
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "leftFrame")))
        time.sleep(1)
        #点击目录名
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/div/ul/li/ul/li[1]/div/span[4]/span'))).click()
        # 切换右边的iframe
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "rightFrame")))

        # 点击修改联系人
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[2]/table/tbody/tr/td[11]/a[1]'))).click()
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        time.sleep(1)

        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).clear()
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).send_keys(Person_name)
        time.sleep(1)

        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'phone'))).clear()
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'phone'))).send_keys(phone)
        time.sleep(1)


        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'company'))).clear()
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'company'))).send_keys(emp)
        time.sleep(1)

        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'job'))).clear()
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'job'))).send_keys(job)
        time.sleep(1)

        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'address'))).clear()
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'address'))).send_keys(job_number)
        time.sleep(1)

        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'email'))).clear()
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'email'))).send_keys(email)
        time.sleep(1)

        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'remark'))).clear()
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'remark'))).send_keys(remark)
        time.sleep(1)

        # 点击保存
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'savebtn'))).click()



        # 修改联系人成功检查
    def test_edit_Person_success(self):
        self.edit_Person(data.Person_name[1], data.phone[3], data.emp[1], data.job[1], data.job_number[1],data.email[2], data.remark[0])
        # 检查保存成功提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.address_info[3], alert)
        time.sleep(10)
        # 关闭提示框
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()



     # 修改系人姓名为空检查
    def test_edit_Person_name_empty(self):
        self.edit_Person(data.Person_name[2],  data.phone[3], data.emp[1], data.job[1], data.job_number[1],data.email[2], data.remark[0])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.address_info[4], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()



    # 修改联系人电话为空检查
    def test_edit_phone_empty(self):
        self.edit_Person(data.Person_name[0], data.phone[2], data.emp[1], data.job[1], data.job_number[1],data.email[0], data.remark[0])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.address_info[6], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()



    # 修改系人电话格式错误检查
    def test_edit_phone_format(self):
        self.edit_Person(data.Person_name[0], data.phone[1], data.emp[1], data.job[1], data.job_number[1],data.email[0], data.remark[0])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.address_info[5], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()



      # 修改系人邮箱格式错误检
    def test_edit_email_format(self):
        self.edit_Person(data.Person_name[0], data.phone[3], data.emp[1], data.job[1], data.job_number[0],data.email[1], data.remark[1])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.address_info[7], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()






    #删除联系人
    def test_delete_Person(self):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        # 切换左边的iframe
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "leftFrame")))
        time.sleep(1)
        # 点击大类名称
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/div/ul/li/ul/li[1]/div/span[4]/span'))).click()

        # 切换右边的iframe
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "rightFrame")))

        #点击删除
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[2]/table/tbody/tr/td[11]/a[2]'))).click()
        #点击确定
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[3]/div/div[2]/div[2]/input[1]'))).click()
        time.sleep(1)
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.address_info[8], alert)
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[3]/div/div[2]/div[2]/input'))).click()



    #删除目录
    def test_delete_Unit(self):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        # 切换左边的iframe
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "leftFrame")))
        time.sleep(1)
        # 点击大类名称
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/div/ul/li/ul/li[1]/div/span[4]/span'))).click()

        # 切换右边的iframe
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "rightFrame")))

        #点击删除
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'deleteUnit'))).click()
        #点击确定
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[3]/div/div[2]/div[2]/input[1]'))).click()
        time.sleep(1)
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.address_info[8], alert)
        time.sleep(10)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[3]/div/div[2]/div[2]/input'))).click()

    #导入
    def test_imp(self):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        # 切换左边的iframe
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "leftFrame")))
        time.sleep(1)
        #点击目录名
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/div/ul/li/ul/li[1]/div/span[4]/span'))).click()
        # 切换右边的iframe
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "rightFrame")))

        # 点击文件导入
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[1]/input[5]'))).click()
        #点击导入文件
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        time.sleep(1)
        #点击浏览
        # WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "file"))).click()
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, "file"))).send_keys('D:\AddressBook.xlsx')
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, "button"))).click()
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(u"成功新增", alert)
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()










if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(Address("test_Address_manage"))
    #添加目录
    suite.addTest(Address("test_add_Unit_success"))
    suite.addTest(Address("test_add_Unit_repeat"))
    suite.addTest(Address("test_add_Unit_empty"))
    #修改目录
    suite.addTest(Address("test_edit_Unit_success"))
    suite.addTest(Address("test_edit_Unit_repeat"))
    suite.addTest(Address("test_edit_Unit_empty"))
    #添加联系人
    suite.addTest(Address("test_add_Person_success"))
    suite.addTest(Address("test_add_Person_name_empty"))
    suite.addTest(Address("test_add_phone_empty"))
    suite.addTest(Address("test_add_phone_format"))
    suite.addTest(Address("test_add_email_format"))
    #修改联系人
    suite.addTest(Address("test_edit_Person_success"))
    suite.addTest(Address("test_edit_Person_name_empty"))
    suite.addTest(Address("test_edit_phone_empty"))
    suite.addTest(Address("test_edit_phone_format"))
    suite.addTest(Address("test_edit_email_format"))
    # 导入
    suite.addTest(Address("test_imp"))
    #  #删除小类
    suite.addTest(Address("test_delete_Person"))

    #     #删除大类
    suite.addTest(Address("test_delete_Unit"))

    runner = unittest.TextTestRunner()
    runner.run(suite)