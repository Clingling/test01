# -*- coding: UTF-8 -*-
from selenium import webdriver
#导入 WebDriverWait 包
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import  WebDriverWait
from selenium.webdriver.support.ui import Select
import time

import Login
import data
import unittest


driver = data.return_driver()

class Deleteknowledgebase(unittest.TestCase):
    def setUp(self):
        pass

    def test_deleteknowledgebase(self):
        try:
            time.sleep(2)
            #进入知识库管理
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.LINK_TEXT, "知识库管理"))).click()
            print "successfully enter the knowledgebase interface"
        except:
            print "can not enter the knowledgebase interface"

    def test_deleteknowledgebasefile_cancel(self):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'leftFrame')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "/html/body/div/div/div/ul/li/ul/li[1]/div/span[4]/span"))).click()
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'rightFrame')))
        time.sleep(2)
        # 点击删除
        WebDriverWait(driver, 10).until( EC.element_to_be_clickable((By.XPATH, "/html/body/div[2]/div[2]/table/tbody/tr/td[9]/a[3]"))).click()
        time.sleep(2)
        # 点击取消
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[3]/div/div[2]/div[2]/input[2]"))).click()
        print "cancel to delete knowledgebasefile"

    def test_deleteknowledgebasefile_success(self):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'leftFrame')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "/html/body/div/div/div/ul/li/ul/li[1]/div/span[4]/span"))).click()
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'rightFrame')))
        time.sleep(2)
        # 点击删除
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, ".//*[@id='DataTable']/tbody/tr/td[9]/a[3]"))).click()
        time.sleep(2)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "html/body/div[3]/div/div[2]/div[2]/input[1]"))).click()
        #关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "html/body/div[3]/div/div[2]/div[2]/input"))).click()
        print "successful delete knowledgebasefile"

    def test_deleteknowledgebase_cancel(self):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'leftFrame')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "/html/body/div/div/div/ul/li/ul/li[1]/div/span[4]/span"))).click()
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'rightFrame')))
        time.sleep(2)
        #点击删除
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'deleteUnit'))).click()
        time.sleep(2)
        #点击取消
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,"html/body/div[3]/div/div[2]/div[2]/input[2]"))).click()
        print "cancel to delete knowledgebase"

    def test_deleteknowledgebase_success(self):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'leftFrame')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "/html/body/div/div/div/ul/li/ul/li[1]/div/span[4]/span"))).click()
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'rightFrame')))
        time.sleep(2)
        #点击删除
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'deleteUnit'))).click()
        time.sleep(2)
        #点击确定
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH,"html/body/div[3]/div/div[2]/div[2]/input[1]"))).click()
        #关闭提示框
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[3]/div/div[2]/div[2]/input"))).click()
        print "successful to delete knowledgebase"

if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(Deleteknowledgebase("test_deleteknowledgebase"))
    suite.addTest(Deleteknowledgebase("test_deleteknowledgebasefile_cancel"))
    suite.addTest(Deleteknowledgebase("test_deleteknowledgebasefile_success"))
    suite.addTest(Deleteknowledgebase("test_deleteknowledgebase_cancel"))
    suite.addTest(Deleteknowledgebase("test_deleteknowledgebase_success"))
    runner = unittest.TextTestRunner()
    runner.run(suite)