# -*- coding:UTF-8 -*-

from selenium import webdriver

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import  WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
import unittest
import time
import data
import re
import Login






driver = data.return_driver()

class LoginOut(unittest.TestCase):
    def setUp(self):
        pass

    def test_loginout(self):
        # driver.quit()
        time.sleep(1)
        # WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "li.logout"))).click()
        article = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[2]/div[1]/div[2]/span[3]/strong")))
        time.sleep(2)
        ActionChains.move_to_element(article).perform()



if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(LoginOut("test_loginout"))
    runner = unittest.TextTestRunner()
    runner.run(suite)