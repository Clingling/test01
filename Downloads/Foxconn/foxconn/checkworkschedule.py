# -*- coding: UTF-8 -*-
from selenium import webdriver
#导入 WebDriverWait 包
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import  WebDriverWait
import time

import Login
import data
import unittest


driver = data.return_driver()

class Checkworkschedule(unittest.TestCase):
    def setUp(self):
        pass

    def test_checkworkschedule(self):
        try:
            time.sleep(2)
            #进入系统管理
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.LINK_TEXT, "系统管理"))).click()
            time.sleep(2)
            #进入作息管理
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.LINK_TEXT, "作息管理"))).click()
            print "successfully enter the workschedule interface"
        except:
            print "can not enter the workschedule interface"

    def test_checkworkschedule_success(self):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        time.sleep(2)
        # 点击查看
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, ".//*[@id='DataTable']/tbody/tr[1]/td[7]/a[1]"))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, '_frame_layer')))
        print "success check workschedule"
        #点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH,".//*[@id='workScheduleLookOnly']/div[2]/input"))).click()

if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(Checkworkschedule("test_checkworkschedule"))
    suite.addTest(Checkworkschedule("test_checkworkschedule_success"))
    runner = unittest.TextTestRunner()
    runner.run(suite)

