# -*- coding:UTF-8 -*-

from selenium import webdriver

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
import unittest
import Login
import time
import data
import re

driver = data.return_driver()

class Notice(unittest.TestCase):
    def setUp(self):
        pass

    def test_notice_manage(self):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        #点击公告管理
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.LINK_TEXT, "公告管理"))).click()

        print "Enter the Notice management interface"



    def add_Notic(self,notic_title,contex):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        #点击添加
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'addNotice'))).click()
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'title'))).send_keys(notic_title)

        #选择时间
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'deadline'))).click()
        #时间控件有一个iframe
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.XPATH, "/html/body/div[2]/iframe")))
       #选择具体日期
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div/div[3]/table/tbody/tr[7]/td[8]"))).click()
       #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'dpOkInput'))).click()
        #返回主iframe
        driver.switch_to_default_content()
        time.sleep(3)
        # 进入添加按钮的iframe
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        time.sleep(3)
        #进入添加输入框的iframe
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        #输入内容
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'context'))).send_keys(contex)
       #点击保存
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[1]/div[2]/input[1]'))).click()



    #添加成功检查
    def test_add_Notic_success(self):
        self.add_Notic(data.notic_title[0],data.context[0])
        #检查保存成功提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.Notic_info[0], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[3]/div/div[2]/div[2]/input'))).click()


  #添加标题重复检查
    def test_add_notic_title_repeat(self):
        self.add_Notic(data.notic_title[0],data.context[0])
        #检查标题重复提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.Notic_info[1], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[3]/div/div[2]/div[2]/input'))).click()
        #点击取消
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[1]/div[2]/input[2]'))).click()



  #添加标题为空检查
    def test_add_notic_title_empty(self):
        self.add_Notic(data.notic_title[2],data.context[0])
        #检查标题为空提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.Notic_info[2], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[3]/div/div[2]/div[2]/input'))).click()
        #点击取消
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[1]/div[2]/input[2]'))).click()


     # 添加内容过长检查
    def test_add_context_length(self):
        self.add_Notic(data.notic_title[1], data.context[1])
        # 检查内容过长提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.Notic_info[3], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[3]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[1]/div[2]/input[2]'))).click()



  #添加内容为空检查
    def test_add_context_empty(self):
        self.add_Notic(data.notic_title[1],data.context[2])
        #检查内容为空提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.Notic_info[4], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[3]/div/div[2]/div[2]/input'))).click()
        #点击取消
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[1]/div[2]/input[2]'))).click()









  #修改公告
    def edit_Notic(self,notic_title,contex):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        #点击修改
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[2]/table/tbody/tr[1]/td[6]/a[2]'))).click()
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        time.sleep(3)
        #清除标题内容
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'title'))).clear()
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'title'))).send_keys(notic_title)
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'context'))).clear()
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'context'))).send_keys(contex)
       #点击保存
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[1]/div[2]/input[1]'))).click()



#   修改成功检查
    def test_edit_Notic_success(self):
        self.edit_Notic(data.notic_title[1],data.context[0])
        #检查保存成功提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.Notic_info[6], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[3]/div/div[2]/div[2]/input'))).click()






  #修改标题重复检查
    def test_edit_notic_title_repeat(self):
        self.edit_Notic(data.notic_title[3],data.context[0])
        #检查标题重复提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.Notic_info[1], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[3]/div/div[2]/div[2]/input'))).click()
        #点击取消
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[1]/div[2]/input[2]'))).click()



  #修改标题为空检查
    def test_edit_notic_title_empty(self):
        self.edit_Notic(data.notic_title[2],data.context[0])
        #检查标题重复提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.Notic_info[2], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[3]/div/div[2]/div[2]/input'))).click()
        #点击取消
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[1]/div[2]/input[2]'))).click()


     # 修改内容过长检查
    def test_edit_context_length(self):
        self.edit_Notic(data.notic_title[1], data.context[1])
        # 检查内容过长提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.Notic_info[3], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[3]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[1]/div[2]/input[2]'))).click()



  #添加内容为空检查
    def test_edit_context_empty(self):
        self.edit_Notic(data.notic_title[1],data.context[2])
        #检查内容为空提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.Notic_info[4], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[3]/div/div[2]/div[2]/input'))).click()
        #点击取消
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[1]/div[2]/input[2]'))).click()





  #查看公告
    def test_check_Notic(self):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        #点击修改
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[2]/table/tbody/tr[1]/td[6]/a[1]'))).click()
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        time.sleep(3)
        #获取标题输入框的值
        title_input =driver.find_element_by_id('title').get_attribute('value')
        print title_input
        self.assertIn(u'标题2', title_input)
        time.sleep(2)
        #获取公告内容的值
        context_textarea = driver.find_element_by_id('context').get_attribute('value')
        print context_textarea
        self.assertIn(u'内容', context_textarea)
       #点击关闭
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[1]/div[2]/input'))).click()




  #删除公告
    def test_delete_Notic(self):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        #点击删除
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[2]/table/tbody/tr[1]/td[6]/a[3]'))).click()
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[4]/div/div[2]/div[2]/input[1]'))).click()

        # time.sleep(3)
        # driver.switch_to_default_content()
        # time.sleep(3)
        # WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        # time.sleep(2)
        time.sleep(1)
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.Notic_info[7], alert)
        time.sleep(10)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[4]/div/div[2]/div[2]/input'))).click()



if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(Notice("test_notice_manage"))
    #添加
    suite.addTest(Notice("test_add_Notic_success"))
    suite.addTest(Notice("test_add_notic_title_repeat"))
    suite.addTest(Notice("test_add_notic_title_empty"))
    suite.addTest(Notice("test_add_context_length"))
    suite.addTest(Notice("test_add_context_empty"))
   #修改
    suite.addTest(Notice("test_edit_Notic_success"))
    suite.addTest(Notice("test_edit_notic_title_repeat"))
    suite.addTest(Notice("test_edit_notic_title_empty"))
    suite.addTest(Notice("test_edit_context_length"))
    suite.addTest(Notice("test_edit_context_empty"))
   #   #查看
    suite.addTest(Notice("test_check_Notic"))
    #删除
    suite.addTest(Notice("test_delete_Notic"))
    runner = unittest.TextTestRunner()
    runner.run(suite)