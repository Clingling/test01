# -*- coding:UTF-8 -*-

from selenium import webdriver

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.ui import Select
import unittest
import Login
import time
import data
import re

driver = data.return_driver()

class Dictionaries(unittest.TestCase):
    def setUp(self):
        pass

    def test_Dictionaries(self):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        #点击系统管理
        # WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[1]/div[2]/ul/li[17]/span/a/samp"))).click()
        time.sleep(1)
        #点击字典表管理
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.LINK_TEXT, "字典表管理"))).click()

        print "Enter the Dictionaries interface"



    def add_Dictionaries(self , dictionaries_name , dictionaries_value,dictionaries_description):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        #点击添加
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'btn_add'))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        # #点击类别
        # time.sleep(2)
        # WebDriverWait(driver, 10).until
        # time.sleep(1)(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[2]/div[1]/span/select'))).click()
        # #类别选择外呼结果
        # WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[2]/div[1]/span/select/option[2]'))).click()
        time.sleep(1)
        Select(driver.find_element_by_id("type")).select_by_visible_text(u"外呼结果")
        #输入名称
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).send_keys(dictionaries_name)

        #输入数值
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'value'))).send_keys(dictionaries_value)
        #输入说明
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'description'))).send_keys(dictionaries_description)
       #点击保存
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[1]'))).click()




    #添加成功检查
    def test_add_Dictionaries_success(self):
        self.add_Dictionaries(data.dictionaries_name[0] , data.dictionaries_value[0] ,data.dictionaries_description[0])
        #检查保存成功提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.dictionaries_info[0], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()





  #添加名称为空检查
    def test_add_dictionaries_name_empty(self):
        self.add_Dictionaries(data.dictionaries_name[2] , data.dictionaries_value[0] ,data.dictionaries_description[0])
        #检查标题为空提示语
        time.sleep(3)
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.dictionaries_info[3], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        time.sleep(2)
        #点击取消
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()


     # 添加名称过长检查
    def test_add_dictionaries_name_length(self):
        self.add_Dictionaries(data.dictionaries_name[3] , data.dictionaries_value[0] ,data.dictionaries_description[0])
        # 检查内容过长提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertEqual(data.dictionaries_info[1], alert)
        # 关闭提示框
        time.sleep(5)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()

    #添加名称重复
    def test_add_dictionaries_name_repeat(self):
        self.add_Dictionaries(data.dictionaries_name[4] , data.dictionaries_value[1] ,data.dictionaries_description[1])
        # 检查名称重复提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertEqual(data.dictionaries_info[2], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()


     # 添加数值过长检查
    def test_add_dictionaries_value_length(self):
        self.add_Dictionaries(data.dictionaries_name[1] , data.dictionaries_value[3] ,data.dictionaries_description[0])
       #添加数值过长提示语检查
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertEqual(data.dictionaries_info[4], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()

  #添加数值为空检查
    def test_add_dictionaries_value_empty(self):
        self.add_Dictionaries(data.dictionaries_name[1] , data.dictionaries_value[2] ,data.dictionaries_description[0])
        #检查数值为空提示语
        time.sleep(3)
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.dictionaries_info[5], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        time.sleep(2)
        #点击取消
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()

     #数值重复检查
    def test_add_dictionaries_value_repeat(self):
        self.add_Dictionaries(data.dictionaries_name[1] , data.dictionaries_value[4] ,data.dictionaries_description[0])
        #检查数值为空提示语
        time.sleep(3)
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.dictionaries_info[6], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        time.sleep(2)
        #点击取消
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()



     # 添加说明过长检查
    def test_add_dictionaries_description_length(self):
        self.add_Dictionaries(data.dictionaries_name[1] , data.dictionaries_value[1] ,data.dictionaries_description[1])
       #添加说明过长提示语检查
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertEqual(data.dictionaries_info[7], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()



    #修改字典表
    def edit_Dictionaries(self,dictionaries_name , dictionaries_value,dictionaries_description):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        #点击查询
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).clear()
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).send_keys(u"字典名称")
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'btn_query'))).click()

        #点击修改
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[2]/table/tbody/tr/td[6]/a[1]'))).click()
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).clear()
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).send_keys(dictionaries_name)

        # 输入数值
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'value'))).clear()
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'value'))).send_keys(dictionaries_value)
        # 输入说明
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'description'))).clear()
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'description'))).send_keys(dictionaries_description)

        time.sleep(2)
        # 点击保存
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[1]'))).click()



#   修改成功检查
    def test_edit_Dictionaries_success(self):
        self.edit_Dictionaries(data.dictionaries_name[1], data.dictionaries_value[1], data.dictionaries_description[0])
        # 检查保存成功提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.dictionaries_info[8], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()



        # 修改名称为空检查
    def test_edit_dictionaries_name_empty(self):
        self.edit_Dictionaries(data.dictionaries_name[2], data.dictionaries_value[0], data.dictionaries_description[0])
        # 检查名称为空提示语
        time.sleep(3)
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.dictionaries_info[3], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        time.sleep(2)
        # 点击取消
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()



        # 修改名称过长检查
    def test_edit_dictionaries_name_length(self):
        self.edit_Dictionaries(data.dictionaries_name[3], data.dictionaries_value[0], data.dictionaries_description[0])
        # 检查名称过长提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertEqual(data.dictionaries_info[1], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()


        # 修改名称重复
    def test_edit_dictionaries_name_repeat(self):
        self.edit_Dictionaries(data.dictionaries_name[4], data.dictionaries_value[0], data.dictionaries_description[0])
        # 检查名称重复提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertEqual(data.dictionaries_info[2], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()


        # 修改数值过长检查
    def test_edit_dictionaries_value_length(self):
        self.edit_Dictionaries(data.dictionaries_name[1], data.dictionaries_value[3], data.dictionaries_description[0])
        # 修改数值过长提示语检查
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertEqual(data.dictionaries_info[4], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()



        #修改数值为空检查
    def test_edit_dictionaries_value_empty(self):
        self.edit_Dictionaries(data.dictionaries_name[1], data.dictionaries_value[2], data.dictionaries_description[0])
        # 检查数值为空提示语
        time.sleep(3)
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.dictionaries_info[5], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        time.sleep(2)
        # 点击取消
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()


        # 修改数值重复检查
    def test_edit_dictionaries_value_repeat(self):
        self.edit_Dictionaries(data.dictionaries_name[1], data.dictionaries_value[4], data.dictionaries_description[0])
        # 检查数值重复提示语
        time.sleep(3)
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.dictionaries_info[6], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        time.sleep(2)
        # 点击取消
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()



        # 修改说明过长检查
    def test_edit_dictionaries_description_length(self):
        self.edit_Dictionaries(data.dictionaries_name[1], data.dictionaries_value[0], data.dictionaries_description[1])
        # 修改说明过长提示语检查
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertEqual(data.dictionaries_info[10], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()


     #删除字典
    def test_delete_Dictionaries(self):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).clear()
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).send_keys(u'字典名称')
        time.sleep(1)
        # 点击查询
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'btn_query'))).click()
        time.sleep(1)
        # 点击删除
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[2]/table/tbody/tr/td[6]/a[2]'))).click()
        time.sleep(1)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[4]/div/div[2]/div[2]/input[1]'))).click()
        time.sleep(1)

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertEqual(data.dictionaries_info[9], alert)

        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[4]/div/div[2]/div[2]/input'))).click()




if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(Dictionaries("test_Dictionaries"))
    #添加
    # suite.addTest(Dictionaries("test_add_Dictionaries_success"))
    # suite.addTest(Dictionaries("test_add_dictionaries_name_empty"))
    # suite.addTest(Dictionaries("test_add_dictionaries_name_length"))
    suite.addTest(Dictionaries("test_add_dictionaries_name_repeat"))
    suite.addTest(Dictionaries("test_add_dictionaries_value_length"))
    suite.addTest(Dictionaries("test_add_dictionaries_value_empty"))
    suite.addTest(Dictionaries("test_add_dictionaries_value_repeat"))
    suite.addTest(Dictionaries("test_add_dictionaries_description_length"))

  # 修改
    suite.addTest(Dictionaries("test_edit_Dictionaries_success"))
    suite.addTest(Dictionaries("test_edit_dictionaries_name_empty"))
    suite.addTest(Dictionaries("test_edit_dictionaries_name_length"))
    suite.addTest(Dictionaries("test_edit_dictionaries_name_repeat"))
    suite.addTest(Dictionaries("test_edit_dictionaries_value_length"))
    suite.addTest(Dictionaries("test_edit_dictionaries_value_empty"))
    suite.addTest(Dictionaries("test_edit_dictionaries_value_repeat"))
    suite.addTest(Dictionaries("test_edit_dictionaries_description_length"))

   #  #删除
    suite.addTest(Dictionaries("test_delete_Dictionaries"))
    runner = unittest.TextTestRunner()
    runner.run(suite)