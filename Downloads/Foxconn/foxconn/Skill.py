# -*- coding:UTF-8 -*-

from selenium import webdriver

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.ui import Select
import unittest
import Login
import time
import data
import re

driver = data.return_driver()

class Skill(unittest.TestCase):
    def setUp(self):
        pass

    def test_Skill(self):
        time.sleep(1)
        driver.switch_to_default_content()
        time.sleep(1)
        #点击技能组管理
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.LINK_TEXT, "技能组管理"))).click()

        print "Enter the Skill interface"



    def add_Skill(self , skillId , skill_name , flag1 , flag2 , flag3):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        #点击添加
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'btn_add'))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        #输入名称
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'skillId'))).send_keys(skillId)
        # 输入数值
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).send_keys(skill_name)
        #选择坐席
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[2]/div[2]/div/div/div[1]/select/option[13]'))).click()
        time.sleep(2 )
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[2]/div[2]/div/div/div[2]/button[1]'))).click()
        #输入说明
        #作息安排
        time.sleep(1)
        Select(driver.find_element_by_id("workScheduleId")).select_by_visible_text("test")

        #转接方式
        time.sleep(1)
        Select(driver.find_element_by_id("transFlag")).select_by_visible_text(u"转技能")
        #z转技能
        time.sleep(1)
        Select(driver.find_element_by_id("changeSkill")).select_by_visible_text(u"金融")
        #业务大类
        time.sleep(1)
        Select(driver.find_element_by_id("businessTypeId")).select_by_visible_text(u"应新应革")
        #备用电话
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'flag1'))).send_keys(flag1)
        #备注一
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'flag2'))).send_keys(flag2)
        #备注二
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'flag3'))).send_keys(flag3)

       #点击保存
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[1]'))).click()




    #添加成功检查
    def test_add_Skill_success(self):
        self.add_Skill(data.skillId[0],data.skill_name[0] , data.flag1[0] , data.flag2[0] , data.flag3[0])

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.skill_info[0], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()


     #技能ID格式错误
    def test_add_skillId_format(self):
        self.add_Skill(data.skillId[1],data.skill_name[0] , data.flag1[0] , data.flag2[0] , data.flag3[0])

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.skill_info[1], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        #点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()


    #技能ID为空
    def test_add_skillId_empty(self):
        self.add_Skill(data.skillId[2],data.skill_name[0] , data.flag1[0] , data.flag2[0] , data.flag3[0])

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.skill_info[2], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        #点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()

    #技能ID重复
    def test_add_skillId_repeat(self):
        self.add_Skill(data.skillId[3],data.skill_name[0] , data.flag1[0] , data.flag2[0] , data.flag3[0])

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.skill_info[3], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        #点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()

       #技能名称为空
    def test_add_skill_name_empty(self):
        self.add_Skill(data.skillId[4],data.skill_name[1] , data.flag1[0] , data.flag2[0] , data.flag3[0])

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.skill_info[4], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        #点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()

       #技能名称重复
    def test_add_skill_name_repeat(self):
        self.add_Skill(data.skillId[4],data.skill_name[0] , data.flag1[0] , data.flag2[0] , data.flag3[0])

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.skill_info[5], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        #点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()


      #备用电话号码格式错误
    def test_add_flag1_empty(self):
        self.add_Skill(data.skillId[4],data.skill_name[3] , data.flag1[1] , data.flag2[0] , data.flag3[0])

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.skill_info[6], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        #点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()

   #修改技能
    def edit_Skill(self  , skill_name , flag1 ):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        #点击修改
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[2]/table/tbody/tr[1]/td[14]/a[1]'))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        #输入名称
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).clear()
        # 输入数值
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).send_keys(skill_name)

        #作息安排
        time.sleep(1)
        Select(driver.find_element_by_id("workScheduleId")).select_by_visible_text("TestC")

        #转接方式
        time.sleep(1)
        Select(driver.find_element_by_id("transFlag")).select_by_visible_text(u"转IVR")
        #z转技能
        time.sleep(1)
        Select(driver.find_element_by_id("transIvr")).select_by_visible_text(u"主流程")
        #业务大类
        time.sleep(1)
        Select(driver.find_element_by_id("businessTypeId")).select_by_visible_text(u"员工关系")
        #备用电话

        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'flag1'))).clear()
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'flag1'))).send_keys(flag1)

       #点击保存
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[1]'))).click()

    #修改成功检查
    def test_edit_Skill_success(self):
        self.edit_Skill(data.skill_name[3] , data.flag1[2] )

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.skill_info[7], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()

     #修改技能名称为空
    def test_edit_skill_name_empty(self):
        self.edit_Skill(data.skill_name[1] , data.flag1[0] )
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.skill_info[4], alert)
        # 关闭提示框
        time.sleep(2)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        #点击取消
        time.sleep(10)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()

     #修改技能名称重复
    def test_edit_skill_name_repeat(self):
        self.edit_Skill(data.skill_name[2] , data.flag1[0])

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.skill_info[5], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        #点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()

     #修改备用电话格式错误
    def test_edit_flag1_empty(self):
        self.edit_Skill(data.skill_name[0] , data.flag1[1])

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.skill_info[6], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        #点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()

     #删除技能组
    def test_delete_Skill(self):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        #点击删除
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[2]/table/tbody/tr[1]/td[ 14]/a[2]'))).click()
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[3]/div/div[2]/div[2]/input[1]'))).click()

        time.sleep(1)
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertEqual(data.skill_info[8], alert)
        time.sleep(10)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[3]/div/div[2]/div[2]/input'))).click()



if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(Skill("test_Skill"))
    #添加
    # suite.addTest(Skill("test_add_Skill_success"))
    # suite.addTest(Skill("test_add_skillId_format"))
    # suite.addTest(Skill("test_add_skillId_empty"))
    # suite.addTest(Skill("test_add_skillId_repeat"))
    # suite.addTest(Skill("test_add_skill_name_empty"))
    # suite.addTest(Skill("test_add_skill_name_repeat"))
    # suite.addTest(Skill("test_add_flag1_empty"))
   # #修改
    suite.addTest(Skill("test_edit_Skill_success"))
    suite.addTest(Skill("test_edit_skill_name_empty"))
    suite.addTest(Skill("test_edit_skill_name_repeat"))
    suite.addTest(Skill("test_edit_flag1_empty"))
    #删除
    suite.addTest(Skill("test_delete_Skill"))
    runner = unittest.TextTestRunner()
    runner.run(suite)

    '/html/body/div/div/div/ul/li/ul/li[1]/ul/li/div/span[5]/span'