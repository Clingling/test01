# -*- coding: UTF-8 -*-
from selenium import webdriver
#导入 WebDriverWait 包
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import  WebDriverWait
import time

import Login
import data
import unittest


driver = data.return_driver()

class Addlocation(unittest.TestCase):
    def setUp(self):
        pass

    def test_addlocation(self):
        try:
            time.sleep(2)
            driver.switch_to_default_content()
            #进入归属地管理
            WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.LINK_TEXT, "归属地管理"))).click()
            print "successfully enter the location interface"
        except:
            print "can not enter the location interface"

    def addLocation(self,comparePhone,area):
        time.sleep(2)
        driver.switch_to_default_content()
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID,'main')))
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,".//*[@id='queryDiv']/li[3]/span/input[3]"))).click()
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID,'_frame_layer')))
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,"comparePhone"))).clear()
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,"comparePhone"))).send_keys(comparePhone)
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,"area"))).clear()
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,"area"))).send_keys(area)
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,"html/body/div[1]/div[2]/input[1]"))).click()
        time.sleep(5)

    def test_addLocation_success(self):
        self.addLocation(data.comparePhone[0],data.area[0])
        alert = WebDriverWait(driver, 10).until(lambda x:x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_location_info[0],alert)
        #关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH,"//input[starts-with(@id,'btn_')]"))).click()

    def test_addLocation_repeat(self):
        self.addLocation(data.comparePhone[1],data.area[1])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_location_info[1],alert)
        #关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()

    def test_addLocation_comparePhone_blank(self):
        self.addLocation(data.comparePhone[2],data.area[2])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_location_info[2], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH,"html/body/div[1]/div[2]/input[2]"))).click()

    def test_addLocation_comparePhone_error(self):
        self.addLocation(data.comparePhone[3], data.area[3])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_location_info[3], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        #点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[2]/input[2]"))).click()

    def test_addLocation_area_blank(self):
        self.addLocation(data.comparePhone[4], data.area[4])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_location_info[4], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        #点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[2]/input[2]"))).click()

    def test_addLocation_area_error(self):
        self.addLocation(data.comparePhone[5], data.area[5])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_location_info[5], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()

if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(Addlocation("test_addlocation"))
    suite.addTest(Addlocation("test_addLocation_success"))
    suite.addTest(Addlocation("test_addLocation_repeat"))
    suite.addTest(Addlocation("test_addLocation_comparePhone_blank"))
    suite.addTest(Addlocation("test_addLocation_comparePhone_error"))
    suite.addTest(Addlocation("test_addLocation_area_blank"))
    suite.addTest(Addlocation("test_addLocation_area_error"))
    runner = unittest.TextTestRunner()
    runner.run(suite)


