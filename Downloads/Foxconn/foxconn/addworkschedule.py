# -*- coding: UTF-8 -*-
from selenium import webdriver
#导入 WebDriverWait 包
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import  WebDriverWait
import time

import Login
import data
import unittest


driver = data.return_driver()

class Addworkschedule(unittest.TestCase):
    def setUp(self):
        pass

    def test_addworkschedule(self):
        try:
            time.sleep(2)
            #进入系统管理
            driver.switch_to_default_content()
            # WebDriverWait(driver, 10).until(
            #     EC.element_to_be_clickable((By.XPATH, ".//*[@id='a0013']/a/samp"))).click()
            # time.sleep(2)
            #进入作息管理
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.LINK_TEXT, "作息管理"))).click()
            print "successfully enter the workschedule interface"
        except:
            print "can not enter the workschedule interface"

    def addworkschedule(self,workschedulename,startTime,releaseTime):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        time.sleep(2)
        #点击添加
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, "btn_add"))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, '_frame_layer')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'name'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'name'))).send_keys(workschedulename)
        time.sleep(2)
        #选择起止日期
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'startTime'))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'startTime'))).send_keys(startTime)
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'releaseTime'))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'releaseTime'))).send_keys(releaseTime)
        time.sleep(2)
        #点击保存
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,"html/body/div[1]/div[2]/input[1]"))).click()

    def test_addworkschedule_success(self):
        self.addworkschedule(data.workschedulename[0],data.startTime[0],data.releaseTime[0])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_workschedule_info[0], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()

    def test_addworkschedule_repeat(self):
        self.addworkschedule(data.workschedulename[1],data.startTime[1],data.releaseTime[1])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_workschedule_info[1], alert)
        # 关闭提示框
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[2]/input[2]"))).click()

    def test_addworkschedule_name_blank(self):
        self.addworkschedule(data.workschedulename[2],data.startTime[2],data.releaseTime[2])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_workschedule_info[2], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        #点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[2]/input[2]"))).click()

    def test_addworkschedule_startTime_blank(self):
        self.addworkschedule(data.workschedulename[3],data.startTime[3],data.releaseTime[3])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_workschedule_info[3], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        #点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[2]/input[2]"))).click()

    def test_addworkschedule_releaseTime_blank(self):
        self.addworkschedule(data.workschedulename[4],data.startTime[4],data.releaseTime[4])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_workschedule_info[4], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        #点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[2]/input[2]"))).click()


if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(Addworkschedule("test_addworkschedule"))
    suite.addTest(Addworkschedule("test_addworkschedule_success"))
    suite.addTest(Addworkschedule("test_addworkschedule_repeat"))
    suite.addTest(Addworkschedule("test_addworkschedule_name_blank"))
    suite.addTest(Addworkschedule("test_addworkschedule_startTime_blank"))
    suite.addTest(Addworkschedule("test_addworkschedule_releaseTime_blank"))
    runnner = unittest.TextTestRunner()
    runnner.run(suite)

