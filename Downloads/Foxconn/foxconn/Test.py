# -*- coding: UTF-8 -*-
from selenium import webdriver
#导入 WebDriverWait 包
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import  WebDriverWait
from selenium.webdriver.support.ui import Select
import time

import Login
import addactioninfo,addknowledgebase,addlocation,addmodule,addoperator,addrole,addworkschedule,checkworkschedule,deleteactioninfo
import deleteknowledgebase,deletelocation,deletemodule,deleterole,deleteworkschedule,editactioninfo,editknowledgebase,editlocation
import editmodule,editoperator,editrole,editworkschedule,importknowledgebase,importlocation,deleteoperator
import data
import unittest
import HTMLTestRunner

driver = data.return_driver()

if __name__ == "__main__":
    #实例化测试套件
    suite = unittest.TestSuite()
    #将测试用例加载到测试套件中

    #登录
    suite.addTest(Login.Login("test_login_success"))

    #知识库管理
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebase"))
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebase_success"))
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebase_repeat"))
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebase_blank"))
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebase_error"))
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebasefile_success"))
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebasefile_repeat"))
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebasefile_name_error"))
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebasefile_name_blank"))
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebasefile_title_blank"))
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebasefile_releaseUnit_blank"))
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebasefile_content_blank"))
    suite.addTest(editknowledgebase.Editknowledgebase("test_editknowledgebase_success"))
    suite.addTest(editknowledgebase.Editknowledgebase("test_editknowledgebase_blank"))
    suite.addTest(editknowledgebase.Editknowledgebase("test_editknowledgebasefile_success"))
    suite.addTest(editknowledgebase.Editknowledgebase("test_editknowledgebasefile_blank"))
    suite.addTest(importknowledgebase.Importknowledgebase("test_importknowledgebase_success"))
    suite.addTest(deleteknowledgebase.Deleteknowledgebase("test_deleteknowledgebasefile_cancel"))
    suite.addTest(deleteknowledgebase.Deleteknowledgebase("test_deleteknowledgebasefile_success"))
    suite.addTest(deleteknowledgebase.Deleteknowledgebase("test_deleteknowledgebase_cancel"))
    suite.addTest(deleteknowledgebase.Deleteknowledgebase("test_deleteknowledgebase_success"))
    print "success"

    #归属地管理
    suite.addTest(addlocation.Addlocation("test_addlocation"))
    suite.addTest(addlocation.Addlocation("test_addLocation_success"))
    suite.addTest(addlocation.Addlocation("test_addLocation_repeat"))
    suite.addTest(addlocation.Addlocation("test_addLocation_comparePhone_blank"))
    suite.addTest(addlocation.Addlocation("test_addLocation_comparePhone_error"))
    suite.addTest(addlocation.Addlocation("test_addLocation_area_blank"))
    suite.addTest(addlocation.Addlocation("test_addLocation_area_error"))
    suite.addTest(editlocation.Editlocation("test_editLocation_success"))
    suite.addTest(editlocation.Editlocation("test_editLocation_blank"))
    suite.addTest(importlocation.Importlocation("test_Importlocation_success"))
    suite.addTest(deletelocation.Deletelocation("test_deletelocation_cancel"))
    suite.addTest(deletelocation.Deletelocation("test_deletelocation_success"))

    # 权限项管理
    suite.addTest(addactioninfo.Addactioninfo("test_addactioninfo"))
    suite.addTest(addactioninfo.Addactioninfo("test_addActioninfo_success"))
    suite.addTest(addactioninfo.Addactioninfo("test_addActioninfo_repeat"))
    suite.addTest(addactioninfo.Addactioninfo("test_addActioninfo_name_error"))
    suite.addTest(addactioninfo.Addactioninfo("test_addActioninfo_name_blank"))
    suite.addTest(addactioninfo.Addactioninfo("test_addActioninfo_description_error"))
    suite.addTest(addactioninfo.Addactioninfo("test_addActioninfo_description_blank"))
    suite.addTest(addactioninfo.Addactioninfo("test_addActioninfo_action_blank"))
    suite.addTest(editactioninfo.Editactioninfo("test_editActioninfo_success"))
    suite.addTest(editactioninfo.Editactioninfo("test_editActioninfo_blank"))
    suite.addTest(deleteactioninfo.Deleteactioninfo("test_deleteactioninfo_cancel"))
    suite.addTest(deleteactioninfo.Deleteactioninfo("test_deleteactioninfo_success"))

    #模块管理
    suite.addTest(addmodule.Addmodule("test_addmodule"))
    suite.addTest(addmodule.Addmodule("test_addModule_success"))
    suite.addTest(addmodule.Addmodule("test_addModule_repeat"))
    suite.addTest(addmodule.Addmodule("test_addModule_name_error"))
    suite.addTest(addmodule.Addmodule("test_addModule_name_blank"))
    suite.addTest(addmodule.Addmodule("test_addModule_description_error"))
    suite.addTest(addmodule.Addmodule("test_addModule_description_blank"))
    suite.addTest(editmodule.Editmodule("test_editModule_success"))
    suite.addTest(editmodule.Editmodule("test_editModule_blank"))
    suite.addTest(deletemodule.Deletemodule("test_deletemodule_cancel"))
    suite.addTest(deletemodule.Deletemodule("test_deletemodule_success"))

    #操作员管理
    suite.addTest(addoperator.Addoperator("test_addoperator"))
    suite.addTest(addoperator.Addoperator("test_addoperator_success"))
    suite.addTest(addoperator.Addoperator("test_addoperator_repeat"))
    suite.addTest(addoperator.Addoperator("test_addoperator_account_error"))
    suite.addTest(addoperator.Addoperator("test_addoperator_account_blank"))
    suite.addTest(addoperator.Addoperator("test_addoperator_operatorname_error"))
    suite.addTest(addoperator.Addoperator("test_addoperator_operatorname_blank"))
    suite.addTest(addoperator.Addoperator("test_addoperator_operatorpassword_error"))
    suite.addTest(addoperator.Addoperator("test_addoperator_operatorpassword_blank"))
    suite.addTest(addoperator.Addoperator("test_addoperator_operatorpasswordTwo_error"))
    suite.addTest(addoperator.Addoperator("test_addoperator_operatorpasswordTwo_blank"))
    suite.addTest(addoperator.Addoperator("test_addoperator_agentNumber_error"))
    suite.addTest(addoperator.Addoperator("test_addoperator_agentNumber_blank"))
    suite.addTest(addoperator.Addoperator("test_addoperator_agentIp_error"))
    suite.addTest(addoperator.Addoperator("test_addoperator_agentIp_blank"))
    suite.addTest(editoperator.Editoperator("test_editoperator_success"))
    suite.addTest(editoperator.Editoperator("test_editoperator_blank"))
    suite.addTest(deleteoperator.Deleteoperator("test_deleteoperator_cancel"))
    suite.addTest(deleteoperator.Deleteoperator("test_deleteoperator_success"))

    #角色管理
    suite.addTest(addrole.Addrole("test_addrole"))
    suite.addTest(addrole.Addrole("test_addrole_success"))
    suite.addTest(addrole.Addrole("test_addrole_repeat"))
    suite.addTest(addrole.Addrole("test_addrole_name_error"))
    suite.addTest(addrole.Addrole("test_addrole_name_blank"))
    suite.addTest(addrole.Addrole("test_addrole_power_blank"))
    suite.addTest(editrole.Editrole("test_editrole_success"))
    suite.addTest(editrole.Editrole("test_editrole_blank"))
    suite.addTest(deleterole.Deleterole("test_deleterole_cancel"))
    suite.addTest(deleterole.Deleterole("test_deleterole_success"))

    #作息管理
    suite.addTest(addworkschedule.Addworkschedule("test_addworkschedule"))
    suite.addTest(addworkschedule.Addworkschedule("test_addworkschedule_success"))
    suite.addTest(addworkschedule.Addworkschedule("test_addworkschedule_repeat"))
    suite.addTest(addworkschedule.Addworkschedule("test_addworkschedule_name_blank"))
    suite.addTest(addworkschedule.Addworkschedule("test_addworkschedule_startTime_blank"))
    suite.addTest(addworkschedule.Addworkschedule("test_addworkschedule_releaseTime_blank"))
    suite.addTest(editworkschedule.Editworkschedule("test_editworkschedule_success"))
    suite.addTest(editworkschedule.Editworkschedule("test_editworkschedule_blank"))
    suite.addTest(deleteworkschedule.Deleteworkschedule("test_deleteworkschedule_cancel"))
    suite.addTest(deleteworkschedule.Deleteworkschedule("test_deleteworkschedule_success"))

    runner = unittest.TextTestRunner()
    runner.run(suite)