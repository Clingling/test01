# -*- coding:UTF-8 -*-

from selenium import webdriver

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
import unittest
import Login
import time
import data
import re

driver = data.return_driver()

class Management(unittest.TestCase):
    def setUp(self):
        pass

    def test_Management(self):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        #点击短信管理
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.LINK_TEXT, "短信管理"))).click()
        time.sleep(1)
        #点击短信模版管理
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.LINK_TEXT, "短信模板管理"))).click()

        print "Enter the sms management interface"



    def add_Message(self , messageName , messageContext):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        #点击添加
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[1]/ul/li[3]/span/input[2]'))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'messageName'))).send_keys(messageName)
        time.sleep(2)
        #输入内容
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'messageContext'))).send_keys(messageContext)
       #点击保存
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[1]'))).click()




    #添加成功检查
    def test_add_Message_success(self):
        self.add_Message(data.messageName[0],data.messageContext[0])
        #检查保存成功提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.message_info[0], alert)
        # 关闭提示框
        time.sleep(5)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()





  #添加短信模板标题为空检查
    def test_add_messageName_empty(self):
        self.add_Message(data.messageName[2], data.messageContext[0])
        #检查标题为空提示语
        time.sleep(3)
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.message_info[1], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        time.sleep(2)
        #点击取消
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()


     # 添加内容过长检查
    def test_add_messageContext_length(self):
        self.add_Message(data.messageName[0], data.messageContext[1])
        # 检查内容过长提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertEqual(data.message_info[3], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()


  #添加内容为空检查
    def test_add_messageContext_empty(self):
        self.add_Message(data.messageName[0], data.messageContext[2])
        #检查内容为空提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertEqual(data.message_info[2], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        #点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()









  #修改短信模板
    def edit_Message(self,messageName,messageContext):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        #点击修改
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[2]/table/tbody/tr[1]/td[6]/a[2]'))).click()
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        time.sleep(3)
        #清除标题
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'messageName'))).clear()
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'messageName'))).send_keys(messageName)
        time.sleep(2)
        # 清除内容
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'messageContext'))).clear()
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'messageContext'))).send_keys(messageContext)
        time.sleep(2)
        # 点击保存
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[1]'))).click()



#   修改成功检查
    def test_edit_Message_success(self):
        self.edit_Message(data.messageName[1],data.messageContext[3])
        #检查保存成功提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertEqual(data.message_info[4], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()





  #修改标题为空检查
    def test_edit_messageName_empty(self):
        self.edit_Message(data.messageName[2], data.messageContext[0])
        #检查标题为空提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertEqual(data.message_info[1], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        #点击取消
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()


     # 修改内容过长检查
    def test_edit_messageContext_length(self):
        self.edit_Message(data.messageName[0], data.messageContext[1])
        # 检查内容过长提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertEqual(data.message_info[3], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()



  #修改内容为空检查
    def test_edit_messageContext_empty(self):
        self.edit_Message(data.messageName[0], data.messageContext[2])
        #检查内容为空提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertEqual(data.message_info[2], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        #点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()





  #查看短信模板
    def test_check_Message(self):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        #点击查看
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[2]/table/tbody/tr[1]/td[6]/a[1]'))).click()
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        time.sleep(3)
        #获取标题输入框的值
        messageName_input =driver.find_element_by_id('messageName').get_attribute('value')
        print messageName_input
        self.assertIn(u'修改后的短信模板名称', messageName_input)
        time.sleep(2)
        #获取模板内容的值
        messageContext_textarea = driver.find_element_by_id('messageContext').get_attribute('value')
        print messageContext_textarea
        self.assertIn(u'修改后的内容', messageContext_textarea)
       #点击关闭
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input'))).click()




  #删除
    def test_delete_Message(self):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        #点击删除
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[2]/table/tbody/tr[1]/td[6]/a[3]'))).click()
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[4]/div/div[2]/div[2]/input[1]'))).click()

        time.sleep(1)
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertEqual(data.message_info[5], alert)
        time.sleep(10)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[4]/div/div[2]/div[2]/input'))).click()



if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(Management("test_Management"))
    #添加
    suite.addTest(Management("test_add_Message_success"))
    suite.addTest(Management("test_add_messageName_empty"))
    suite.addTest(Management("test_add_messageContext_length"))
    suite.addTest(Management("test_add_messageContext_empty"))
   #修改
    suite.addTest(Management("test_edit_Message_success"))
    # suite.addTest(Management("test_edit_messageName_empty"))
    # suite.addTest(Management("test_edit_messageContext_length"))
    # suite.addTest(Management("test_edit_messageContext_empty"))

   #   查看
    suite.addTest(Management("test_check_Message"))
    #删除
    suite.addTest(Management("test_delete_Message"))
    runner = unittest.TextTestRunner()
    runner.run(suite)