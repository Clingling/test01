# -*- coding: UTF-8 -*-
from selenium import webdriver
#导入 WebDriverWait 包
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import  WebDriverWait
from selenium.webdriver.support.ui import Select
import time

import Login
import data
import unittest


driver = data.return_driver()

class Importknowledgebase(unittest.TestCase):
    def setUp(self):
        pass

    def test_importknowledgebase(self):
        try:
            time.sleep(2)
            #进入知识库管理
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.LINK_TEXT, "知识库管理"))).click()
            print "successfully enter the knowledgebase interface"
        except:
            print "can not enter the knowledgebase interface"

    def test_importknowledgebase_success(self):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'leftFrame')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable(
                (By.XPATH, "html/body/div/div/div/ul/li/ul/li[1]/ul/li/div/span[5]/span"))).click()
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'rightFrame')))
        time.sleep(2)
        # 点击上传附件
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, ".//*[@id='DataTable']/tbody/tr/td[9]/a[1]"))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        time.sleep(2)
        #选择上传文件
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, "file"))).send_keys("D:\zxwl.pdf")
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, "button"))).click()
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        print "successful import knowledgebase"

if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(Importknowledgebase("test_importknowledgebase"))
    suite.addTest(Importknowledgebase("test_importknowledgebase_success"))
    runner = unittest.TextTestRunner()
    runner.run(suite)

