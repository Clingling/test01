# -*- coding:UTF-8 -*-

from selenium import webdriver

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import  WebDriverWait
from selenium.webdriver.support.ui import Select
import HTMLTestRunner
import Login
import Address
import Business
import Dictionaries
import Management
import Notice
import System
import Skill
import unittest
import time
import data
import re
import addactioninfo
import  addknowledgebase
import addlocation
import addmodule
import addoperator
import addrole
import addworkschedule
import checkworkschedule
import deleteactioninfo
import deleteknowledgebase
import deletelocation
import deletemodule
import deleterole
import deleteworkschedule
import editactioninfo
import editknowledgebase
import editlocation
import editmodule
import editoperator
import editrole
import editworkschedule
import importknowledgebase
import importlocation
import deleteoperator
# import editmodule,editoperator,editrole,editworkschedule,importknowledgebase,importlocation,deleteoperator
# import addactioninfo,addknowledgebase,addlocation,addmodule,addoperator,addrole,addworkschedule,checkworkschedule,deleteactioninfo
# import deleteknowledgebase,deletelocation,deletemodule,deleterole,deleteworkschedule,editactioninfo,editknowledgebase,editlocation





driver = data.return_driver()


if __name__ == "__main__":
    suite = unittest.TestSuite()
    #登录
    suite.addTest(Login.Login("test_login_success"))


    #知识库管理
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebase"))
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebase_success"))
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebase_repeat"))
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebase_blank"))
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebase_error"))
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebasefile_success"))
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebasefile_repeat"))
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebasefile_name_error"))
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebasefile_name_blank"))
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebasefile_title_blank"))
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebasefile_releaseUnit_blank"))
    suite.addTest(addknowledgebase.Addknowledgebase("test_addknowledgebasefile_content_blank"))
    suite.addTest(editknowledgebase.Editknowledgebase("test_editknowledgebase_success"))
    suite.addTest(editknowledgebase.Editknowledgebase("test_editknowledgebase_blank"))
    suite.addTest(editknowledgebase.Editknowledgebase("test_editknowledgebasefile_success"))
    suite.addTest(editknowledgebase.Editknowledgebase("test_editknowledgebasefile_blank"))
    # suite.addTest(importknowledgebase.Importknowledgebase("test_importknowledgebase_success"))
    suite.addTest(deleteknowledgebase.Deleteknowledgebase("test_deleteknowledgebasefile_cancel"))
    suite.addTest(deleteknowledgebase.Deleteknowledgebase("test_deleteknowledgebasefile_success"))
    suite.addTest(deleteknowledgebase.Deleteknowledgebase("test_deleteknowledgebase_cancel"))
    suite.addTest(deleteknowledgebase.Deleteknowledgebase("test_deleteknowledgebase_success"))
    print "success"


    #公告管理
    suite.addTest(Notice.Notice("test_notice_manage"))
    # 添加
    suite.addTest(Notice.Notice("test_add_Notic_success"))
    suite.addTest(Notice.Notice("test_add_notic_title_repeat"))
    suite.addTest(Notice.Notice("test_add_notic_title_empty"))
    suite.addTest(Notice.Notice("test_add_context_length"))
    suite.addTest(Notice.Notice("test_add_context_empty"))
    # 修改
    suite.addTest(Notice.Notice("test_edit_Notic_success"))
    suite.addTest(Notice.Notice("test_edit_notic_title_repeat"))
    suite.addTest(Notice.Notice("test_edit_notic_title_empty"))
    suite.addTest(Notice.Notice("test_edit_context_length"))
    suite.addTest(Notice.Notice("test_edit_context_empty"))
    #   #查看
    suite.addTest(Notice.Notice("test_check_Notic"))
    # 删除
    suite.addTest(Notice.Notice("test_delete_Notic"))
    print "success"

    #通讯录管理
    suite.addTest(Address.Address("test_Address_manage"))
    suite.addTest(Address.Address("test_add_Unit_success"))
    suite.addTest(Address.Address("test_add_Unit_repeat"))
    suite.addTest(Address.Address("test_add_Unit_empty"))

    suite.addTest(Address.Address("test_edit_Unit_success"))
    suite.addTest(Address.Address("test_edit_Unit_repeat"))
    suite.addTest(Address.Address("test_edit_Unit_empty"))

    suite.addTest(Address.Address("test_add_Person_success"))
    suite.addTest(Address.Address("test_add_Person_name_empty"))
    suite.addTest(Address.Address("test_add_phone_empty"))
    suite.addTest(Address.Address("test_add_phone_format"))
    suite.addTest(Address.Address("test_edit_Person_success"))
    suite.addTest(Address.Address("test_edit_Person_name_empty"))
    suite.addTest(Address.Address("test_edit_phone_empty"))
    suite.addTest(Address.Address("test_edit_phone_format"))
    suite.addTest(Address.Address("test_edit_email_format"))
    suite.addTest(Address.Address("test_imp"))
    suite.addTest(Address.Address("test_delete_Person"))
    suite.addTest(Address.Address("test_delete_Unit"))
    print "success"


     #业务类型管理
    suite.addTest(Business.Business("test_Business_manage"))
    #添加大类
    suite.addTest(Business.Business("test_add_L_Business_success"))
    suite.addTest(Business.Business("test_add_L_Business_repeat"))
    suite.addTest(Business.Business("test_add_L_Business_empty"))
    #修改大类
    suite.addTest(Business.Business("test_edit_L_Business_success"))
    suite.addTest(Business.Business("test_edit_L_Business_repeat"))
    suite.addTest(Business.Business("test_edit_L_Business_empty"))
    #添加小类
    suite.addTest(Business.Business("test_add_S_Business_success"))
    suite.addTest(Business.Business("test_add_S_Business_success2"))
    suite.addTest(Business.Business("test_add_S_Business_repeat"))
    suite.addTest(Business.Business("test_add_S_Business_empty"))
    #修改小类
    suite.addTest(Business.Business("test_edit_S_Business_success"))
    suite.addTest(Business.Business("test_edit_S_Business_repeat"))
    suite.addTest(Business.Business("test_edit_S_Business_empty"))
    #  #删除小类
    suite.addTest(Business.Business("test_delete_S_Business"))
        #删除大类
    suite.addTest(Business.Business("test_delete_L_Business"))
    print "success"

    # #技能组管理
    # suite.addTest(Skill.Skill("test_Skill"))
    # # # 添加
    # suite.addTest(Skill.Skill("test_add_Skill_success"))
    # suite.addTest(Skill.Skill("test_add_skillId_format"))
    # suite.addTest(Skill.Skill("test_add_skillId_empty"))
    # suite.addTest(Skill.Skill("test_add_skillId_repeat"))
    # suite.addTest(Skill.Skill("test_add_skill_name_empty"))
    # suite.addTest(Skill.Skill("test_add_skill_name_repeat"))
    # suite.addTest(Skill.Skill("test_add_flag1_empty"))
    # # #修改
    # suite.addTest(Skill.Skill("test_edit_Skill_success"))
    # suite.addTest(Skill.Skill("test_edit_skill_name_empty"))
    # suite.addTest(Skill.Skill("test_edit_skill_name_repeat"))
    # suite.addTest(Skill.Skill("test_edit_flag1_empty"))
    # # # 删除
    # suite.addTest(Skill.Skill("test_delete_Skill"))
    # print "success"

     #短信管理
    suite.addTest(Management.Management("test_Management"))
    suite.addTest(Management.Management("test_add_Message_success"))
    suite.addTest(Management.Management("test_add_messageName_empty"))
    suite.addTest(Management.Management("test_add_messageContext_length"))
    suite.addTest(Management.Management("test_add_messageContext_empty"))
    suite.addTest(Management.Management("test_edit_Message_success"))
    suite.addTest(Management.Management("test_edit_messageName_empty"))
    suite.addTest(Management.Management("test_edit_messageContext_length"))
    suite.addTest(Management.Management("test_edit_messageContext_empty"))
    suite.addTest(Management.Management("test_check_Message"))
    suite.addTest(Management.Management("test_delete_Message"))

    #归属地管理
    suite.addTest(addlocation.Addlocation("test_addlocation"))
    suite.addTest(addlocation.Addlocation("test_addLocation_success"))
    suite.addTest(addlocation.Addlocation("test_addLocation_repeat"))
    suite.addTest(addlocation.Addlocation("test_addLocation_comparePhone_blank"))
    suite.addTest(addlocation.Addlocation("test_addLocation_comparePhone_error"))
    suite.addTest(addlocation.Addlocation("test_addLocation_area_blank"))
    suite.addTest(addlocation.Addlocation("test_addLocation_area_error"))
    suite.addTest(editlocation.Editlocation("test_editLocation_success"))
    suite.addTest(editlocation.Editlocation("test_editLocation_blank"))
    suite.addTest(importlocation.Importlocation("test_Importlocation_success"))
    suite.addTest(deletelocation.Deletelocation("test_deletelocation_cancel"))
    suite.addTest(deletelocation.Deletelocation("test_deletelocation_success"))


    #
    # # 权限项管理
    suite.addTest(addactioninfo.Addactioninfo("test_addactioninfo"))
    suite.addTest(addactioninfo.Addactioninfo("test_addActioninfo_success"))
    suite.addTest(addactioninfo.Addactioninfo("test_addActioninfo_repeat"))
    suite.addTest(addactioninfo.Addactioninfo("test_addActioninfo_name_error"))
    suite.addTest(addactioninfo.Addactioninfo("test_addActioninfo_name_blank"))
    suite.addTest(addactioninfo.Addactioninfo("test_addActioninfo_description_error"))
    suite.addTest(addactioninfo.Addactioninfo("test_addActioninfo_description_blank"))
    suite.addTest(addactioninfo.Addactioninfo("test_addActioninfo_action_blank"))
    suite.addTest(editactioninfo.Editactioninfo("test_editActioninfo_success"))
    suite.addTest(editactioninfo.Editactioninfo("test_editActioninfo_blank"))
    suite.addTest(deleteactioninfo.Deleteactioninfo("test_deleteactioninfo_cancel"))
    suite.addTest(deleteactioninfo.Deleteactioninfo("test_deleteactioninfo_success"))
    #
    # #模块管理
    suite.addTest(addmodule.Addmodule("test_addmodule"))
    suite.addTest(addmodule.Addmodule("test_addModule_success"))
    suite.addTest(addmodule.Addmodule("test_addModule_repeat"))
    suite.addTest(addmodule.Addmodule("test_addModule_name_error"))
    suite.addTest(addmodule.Addmodule("test_addModule_name_blank"))
    suite.addTest(addmodule.Addmodule("test_addModule_description_error"))
    suite.addTest(addmodule.Addmodule("test_addModule_description_blank"))
    suite.addTest(editmodule.Editmodule("test_editModule_success"))
    suite.addTest(editmodule.Editmodule("test_editModule_blank"))
    suite.addTest(deletemodule.Deletemodule("test_deletemodule_cancel"))
    suite.addTest(deletemodule.Deletemodule("test_deletemodule_success"))

    # #角色管理
    suite.addTest(addrole.Addrole("test_addrole"))
    suite.addTest(addrole.Addrole("test_addrole_success"))
    suite.addTest(addrole.Addrole("test_addrole_repeat"))
    suite.addTest(addrole.Addrole("test_addrole_name_error"))
    suite.addTest(addrole.Addrole("test_addrole_name_blank"))
    suite.addTest(addrole.Addrole("test_addrole_power_blank"))
    suite.addTest(editrole.Editrole("test_editrole_success"))
    suite.addTest(editrole.Editrole("test_editrole_blank"))
    suite.addTest(deleterole.Deleterole("test_deleterole_cancel"))
    suite.addTest(deleterole.Deleterole("test_deleterole_success"))
    #
    # #操作员管理
    suite.addTest(addoperator.Addoperator("test_addoperator"))
    suite.addTest(addoperator.Addoperator("test_addoperator_success"))
    suite.addTest(addoperator.Addoperator("test_addoperator_repeat"))
    suite.addTest(addoperator.Addoperator("test_addoperator_account_error"))
    suite.addTest(addoperator.Addoperator("test_addoperator_account_blank"))
    suite.addTest(addoperator.Addoperator("test_addoperator_operatorname_error"))
    suite.addTest(addoperator.Addoperator("test_addoperator_operatorname_blank"))
    suite.addTest(addoperator.Addoperator("test_addoperator_operatorpassword_error"))
    suite.addTest(addoperator.Addoperator("test_addoperator_operatorpassword_blank"))
    suite.addTest(addoperator.Addoperator("test_addoperator_operatorpasswordTwo_error"))
    suite.addTest(addoperator.Addoperator("test_addoperator_operatorpasswordTwo_blank"))
    suite.addTest(addoperator.Addoperator("test_addoperator_agentNumber_error"))
    suite.addTest(addoperator.Addoperator("test_addoperator_agentNumber_blank"))
    suite.addTest(addoperator.Addoperator("test_addoperator_agentIp_error"))
    suite.addTest(addoperator.Addoperator("test_addoperator_agentIp_blank"))
    suite.addTest(editoperator.Editoperator("test_editoperator_success"))
    suite.addTest(editoperator.Editoperator("test_editoperator_blank"))
    suite.addTest(deleteoperator.Deleteoperator("test_deleteoperator_cancel"))
    suite.addTest(deleteoperator.Deleteoperator("test_deleteoperator_success"))

   # #配置管理
    suite.addTest(System.System("test_System_manage"))
    suite.addTest(System.System("test_add_System_success"))
    suite.addTest(System.System("test_add_itemName_repeat"))
    suite.addTest(System.System("test_add_itemName_empty"))
    suite.addTest(System.System("test_add_itemName_length"))
    suite.addTest(System.System("test_add_itemValue_empty"))
    suite.addTest(System.System("test_add_itemValue_length"))
    suite.addTest(System.System("test_add_itemValue_format"))
    suite.addTest(System.System("test_add_itemNameCn_empty"))
    suite.addTest(System.System("test_add_itemNameCn_length"))
    suite.addTest(System.System("test_edit_System_success"))
    suite.addTest(System.System("test_edit_itemName_repeat"))
    suite.addTest(System.System("test_edit_itemName_empty"))
    suite.addTest(System.System("test_edit_itemName_length"))
    suite.addTest(System.System("test_edit_itemValue_empty"))
    suite.addTest(System.System("test_edit_itemValue_length"))
    suite.addTest(System.System("test_edit_itemValue_format"))
    suite.addTest(System.System("test_edit_itemNameCn_empty"))
    suite.addTest(System.System("test_edit_itemNameCn_length"))
    suite.addTest(System.System("test_delete_System"))
    print "success"
   #
   #  #字典表管理
    suite.addTest(Dictionaries.Dictionaries("test_Dictionaries"))
    suite.addTest(Dictionaries.Dictionaries("test_add_Dictionaries_success"))
    suite.addTest(Dictionaries.Dictionaries("test_add_dictionaries_name_empty"))
    suite.addTest(Dictionaries.Dictionaries("test_add_dictionaries_name_length"))
    suite.addTest(Dictionaries.Dictionaries("test_add_dictionaries_name_repeat"))
    suite.addTest(Dictionaries.Dictionaries("test_add_dictionaries_value_length"))
    suite.addTest(Dictionaries.Dictionaries("test_add_dictionaries_value_empty"))
    suite.addTest(Dictionaries.Dictionaries("test_add_dictionaries_value_repeat"))
    suite.addTest(Dictionaries.Dictionaries("test_add_dictionaries_description_length"))
    suite.addTest(Dictionaries.Dictionaries("test_edit_Dictionaries_success"))
    suite.addTest(Dictionaries.Dictionaries("test_edit_dictionaries_name_empty"))
    suite.addTest(Dictionaries.Dictionaries("test_edit_dictionaries_name_length"))
    suite.addTest(Dictionaries.Dictionaries("test_edit_dictionaries_name_repeat"))
    suite.addTest(Dictionaries.Dictionaries("test_edit_dictionaries_value_length"))
    suite.addTest(Dictionaries.Dictionaries("test_edit_dictionaries_value_empty"))
    suite.addTest(Dictionaries.Dictionaries("test_edit_dictionaries_value_repeat"))
    suite.addTest(Dictionaries.Dictionaries("test_edit_dictionaries_description_length"))
    suite.addTest(Dictionaries.Dictionaries("test_delete_Dictionaries"))
    print "success"
   #
   #  #作息管理
    suite.addTest(addworkschedule.Addworkschedule("test_addworkschedule"))
    suite.addTest(addworkschedule.Addworkschedule("test_addworkschedule_success"))
    suite.addTest(addworkschedule.Addworkschedule("test_addworkschedule_repeat"))
    suite.addTest(addworkschedule.Addworkschedule("test_addworkschedule_name_blank"))
    suite.addTest(addworkschedule.Addworkschedule("test_addworkschedule_startTime_blank"))
    suite.addTest(addworkschedule.Addworkschedule("test_addworkschedule_releaseTime_blank"))
    suite.addTest(editworkschedule.Editworkschedule("test_editworkschedule_success"))
    suite.addTest(editworkschedule.Editworkschedule("test_editworkschedule_blank"))
    suite.addTest(deleteworkschedule.Deleteworkschedule("test_deleteworkschedule_cancel"))
    suite.addTest(deleteworkschedule.Deleteworkschedule("test_deleteworkschedule_success"))


    # runner = unittest.TextTestRunner()
    # runner.run(suite)
    #

    # 定义个报告存放路径，支持相对路径。
    reportfilename = data.reportfilename
    fp = open(reportfilename, 'wb')
    # 定义测试报告
    runner = HTMLTestRunner.HTMLTestRunner(
            stream=fp,
            title='Report_title',
            description='Report_description')
    # 运行测试用例
    runner.run(suite)
    # 关闭报告文件
    fp.close()
    # data.quit_driver()