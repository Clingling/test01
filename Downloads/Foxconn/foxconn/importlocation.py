# -*- coding: UTF-8 -*-
from selenium import webdriver
#导入 WebDriverWait 包
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import  WebDriverWait
import time

import Login
import data
import unittest


driver = data.return_driver()

class Importlocation(unittest.TestCase):
    def setUp(self):
        pass

    def test_Importlocation(self):
        try:
            time.sleep(2)
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.LINK_TEXT, "归属地管理"))).click()
            print "successfully enter the location interface"
        except:
            print "can not enter the location interface"

    def test_Importlocation_success(self):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH,".//*[@id='queryDiv']/li[3]/span/input[4]"))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID,"_frame_layer")))
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,"file"))).send_keys("D:\Attribution.xlsx")
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,"button"))).click()
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,"//input[starts-with(@id,'btn_')]"))).click()
        print "successful import location"

if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(Importlocation("test_Importlocation"))
    suite.addTest(Importlocation("test_Importlocation_success"))
    runner = unittest.TextTestRunner()
    runner.run(suite)
