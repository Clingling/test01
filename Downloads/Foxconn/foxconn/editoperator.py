# -*- coding: UTF-8 -*-
from selenium import webdriver
#导入 WebDriverWait 包
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import  WebDriverWait
from selenium.webdriver.support.ui import Select
import time

import Login
import data
import unittest


driver = data.return_driver()

class Editoperator(unittest.TestCase):
    def setUp(self):
        pass

    def test_editoperator(self):
        try:
            time.sleep(2)
            # 进入系统管理
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.LINK_TEXT, "系统管理"))).click()
            time.sleep(2)
            # 进入操作员管理
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.LINK_TEXT, "操作员管理"))).click()
            print "successfully enter the operator interface"
        except:
            print "can not enter the operator interface"

    def editoperator(self,account1,operatorname1,operatorpassword1,operatorpasswordTwo1,agentNumber1,agentIp1):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, ".//*[@id='DataTable']/tbody/tr[1]/td[9]/a[1]"))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, '_frame_layer')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'account'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'account'))).send_keys(account1)
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).send_keys(operatorname1)
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'password'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'password'))).send_keys(operatorpassword1)
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'passwordTwo'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'passwordTwo'))).send_keys(
            operatorpasswordTwo1)
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'agentNumber'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'agentNumber'))).send_keys(agentNumber1)
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'agentIp'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'agentIp'))).send_keys(agentIp1)
        time.sleep(2)
        Select(driver.find_element_by_id("role")).select_by_visible_text(u"接听坐席")
        time.sleep(2)
        Select(driver.find_element_by_id("callOut")).select_by_visible_text(u"否")
        time.sleep(2)
        Select(driver.find_element_by_id("agentType")).select_by_visible_text(u"主管")
        time.sleep(2)
        Select(driver.find_element_by_id("parentId")).select_by_visible_text(u"招募经理")
        time.sleep(2)
        Select(driver.find_element_by_id("trunk")).select_by_visible_text(u"郑州中继")
        time.sleep(2)
        Select(driver.find_element_by_id("agentNet")).select_by_visible_text(u"是")
        time.sleep(2)
        Select(driver.find_element_by_id("outAgent")).select_by_visible_text(u"否")
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[1]"))).click()

    def test_editoperator_success(self):
        self.editoperator(data.account1[0], data.operatorname1[0], data.operatorpassword1[0], data.operatorpasswordTwo1[0],
                         data.agentNumber1[0], data.agentIp1[0])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.edit_operator_info[0], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[2]/div[1]/div[2]/div[2]/input"))).click()

    def test_editoperator_blank(self):
        self.editoperator(data.account1[1], data.operatorname1[1], data.operatorpassword1[1], data.operatorpasswordTwo1[1],
                         data.agentNumber1[1], data.agentIp1[1])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.edit_operator_info[1], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[2]/div[1]/div[2]/div[2]/input"))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(Editoperator("test_editoperator"))
    suite.addTest(Editoperator("test_editoperator_success"))
    suite.addTest(Editoperator("test_editoperator_blank"))
    runner = unittest.TextTestRunner()
    runner.run(suite)