# -*- coding:UTF-8 -*-
from selenium import webdriver
#url parameter
url = 'http://192.168.16.245:10128/foxconn/'


driver = webdriver.Firefox()
driver.get(url)
#driver.implicitly_wait(5)
driver.maximize_window()


def return_driver():
    return driver

def quit_driver():
    driver.quit()



#user parameter
userName = 'admin'
password = 'admin'



#g公告标题
notic_title=[u'标题1',u'标题2','','22']
#公告内容
context=[u'内容','qwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww','']
#公告错误提示
Notic_info=[u'保存成功',u'和已有标题重复',u'公告标题不能为空',u'公告内容长度不能超过300个字符',u'公告内容不能为空',u'截止时间不能为空',u'修改成功',u'删除成功!']




#短信模版名称

messageName=[u'短信模板名称' , u'修改后的短信模板名称' , '']
#短信模板内容
messageContext = [u'短信模板内容' , 'qwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww' , '' ,u'修改后的内容']
#短信模板错误提示
message_info = [u'添加成功' , u'短信模板名称不能为空' , u'短信模板内容不能为空' , u'短信模板内容长度不能超过300个字符',u'修改成功' , u'删除成功!']



#配置管理
#配置项名称
itemName=[u'配置项值' , u'修改后的配置项值' , '' ,
          '1222222222222222222222222222222222222222222222222222222432432234234234234234234234234234' , 'CTI_URL']

#配置项值
itemValue=[u'1' , '' , u'中文' ,
           '22222222222222222223333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333322222222222222222222222222222222222222222222333333333333333333333333333333333333333333333333333333333333333333331111111111111111111111111111111111111111111111111111111111111111333333333333333333333333333333312321312312312222222223123']

#配置项描述
itemNameCn=[u'配置项描述' , u'修改后的配置项描述' , '' , u'1222222222222222222222222222222222222222222222222222222432432234234234234234234234234234']


system_info= [u'保存成功！' , u'配置项名称长度不能超过80个字符!', u'配置项名称不能为空！' , u'配置项名称重复，请重新输入！' , u'配置项值不能包含中文!' ,
              u'配置项值长度不能超过256个字符!' , u'配置项值不能为空!' , u'配置项描述长度不能超过80个字符!' , u'配置项描述不能为空!' , u'删除成功!']


#业务类型管理
L_name = [u'大类' , '' , u'修改后的大类', u'IT服务']
S_name = [u'小类' , '' , u'修改后的小类', u'重复的小类', '']


business_info = [u'添加成功',u'同级业务类型名称不能重复', u'修改成功' ,u'类型名不能为空' , u'删除成功!']


#通讯录管理
Unit_name = [u'目录' , u'修改后的目录'  , '', u'北京']
Person_name = [u'姓名', u'修改后的姓名' , '']
phone = ['18585859696' , 'qwer' , '', '13569698585']
emp =[u'部门' , u'修改后的部门']
job =[u'职务' , u'修改后的职务']
job_number = ['123' , '321']
email= ['1245@qq.com' , 'qwqweqw' ,'987654@qq.com']
remark = [u'备注' , u'修改后的备注']

path = 'E:\addressBook'

address_info = [u'添加成功',u'目录名称不能为空', u'目录名称不能重复' , u'修改成功', u'姓名不能为空',u'请输入正确的电话号码,3至16位数字', u'电话号码不能为空' ,
                u'邮箱格式不正确' , u'删除成功!' ]



#技能组管理
#技能ID
skillId = ['456' , u'技能ID' , '' , '391412' , '789']
#技能名称
skill_name = [u'技能名称' , '' , u'金融' ,u'修改后的技能名称']
#备用电话
flag1 =['18596968574' , u'电话' , '13569698585']
#备注一
flag2= [u'备注一' ]
#备注二
flag3 = [u'备注二']

skill_info = [u'添加成功' , u'请输入9位以内有效数字', u'技能ID不能为空' ,  u'技能ID不能重复' , u'技能名称不能为空' , u'技能名称不能重复' ,
              u'备用电话号码错误：请输入正确的电话号码,3至16位数字' ,u'修改成功' , u'删除成功!' ]


#字典表管理
#字典名称
dictionaries_name = [u'字典名称' , u'修改后的字典名称' , '' , '12345678901234567890123' , u'办理']
#数值
dictionaries_value =[u'数值' , u'修改后的数值' , '' , '123456789012345678901234567890123456','1']
#说明
dictionaries_description = [u'说明'  ,
               u'喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂喂切切切切切切切切切切切切切切切切切切切切切切切切切切切切切切切切切']
#提语示
dictionaries_info = [u'保存成功！', u'名称长度超长！,名称长度不能超过20个字符' , u'同一类别下名称或数值重复，请重新输入！' , u'名称不能为空！' ,u'数值长度超长！,数值长度不能超过30个字符' ,
                     u'数值不能为空！' , u'同一类别下名称或数值重复，请重新输入！' , u'说明长度超长,说明长度不能超过80个字符' , u'修改成功！' , u'删除成功!' ,
                     u'说明长度超长！,说明长度不能超过80个字符']



#location parameter
comparePhone = ['1231231','1231231','','-1AA@','1231232','1231233']
area = [u'湖北武汉',u'湖北武汉',u'湖北武汉',u'湖北武汉','',u'#@@，——*&……%ddd问题dddddddddddddddddddddd']
add_location_info = [u'保存成功',u'保存成功',u'电话号码不能为空',
                     u'请填写正确的电话号码,7位数字',u'归属地不能为空',u'保存成功']
area1 = [u'湖北荆州','']
edit_location_info = [u'修改成功',u'归属地不能为空']

#actioninfo parameter
name = ['AddOperator','AddOperator','@@@!!--','','AddOperator1','AddOperator2','AddOperator3']
description = [u'添加操作员',u'添加操作员',u'添加操作员1',u'添加操作员2',
               u'添加操作员添加操作员添加操作员添加添加操作员操作员','',u'添加操作员3']
action = ['com/zxwl','com/zxwl','com/zxwl/1','com/zxwl/2','com/zxwl/3','com/zxwl/4','']
add_actioninfo = [u'保存成功！',u'权限项名称重复，请重新输入！',u'权限项名称不能包含除“-_”外的特殊字符！',
                  u'权限项名称不能为空!',u'说明过长!,说明长度不能超过20个字符',u'说明不能为空!',u'权限项URL不能为空!']
edit_actioninfo = [u'保存成功！',u'权限项名称不能为空!']
name1 = ['AddOperator4','']
description1 = [u'添加操作员4','']
action1 = ['com/zxwl/5','']

#module parameter
modulename = ['test','test','test@','','testA','testB']
moduledescription = ['test','test','test1','test2','123456789012345678901','']
add_module_info = [u'保存成功！',u'模块名称重复，请重新输入！',u'模块名称不能包含除“-_”外的特殊字符！',
                   u'模块名称不能为空！',u'模块描述超长,模块描述长度不能超过20个字符',u'模块描述不能为空！']

modulename1 = ['testC','']
moduledescription1 = ['testC','']
edit_module_info = [u'修改成功！',u'模块名称不能为空！']

#role parameter
rolename = ['Test','Test',u'!@#$%','',]
roledescription = ['Test','Test','Test1','Test2']
add_role_info = [u'保存成功！',u'角色名称重复，请重新输入！',u'角色名称不能包含除“-_”外的特殊字符！',u'角色名称不能为空！',u'权限配置不能为空！']
rolename1 = ['TestA','']
roledescription1 = ['TestA','']
edit_role_info = [u'修改成功！',u'角色名称不能为空！']

#operator parameter
account = ['testadd','testadd','testaddoperator','','testadd3','testadd4','testadd5',
           'testadd6','testadd7','testadd8','testadd9','testadd10','testadd11','testadd15']
operatorname = ['testadd','testadd','testadd1','testadd2',u'@!','','testadd5',
                'testadd6','testadd7','testadd8','testadd9','testadd10','testadd11','testadd15']
operatorpassword = ['testadd','testadd','testadd1','testadd2','testadd3','testadd4','12345678901234567','',
                    'testadd7','testadd8','testadd9','testadd10','testadd11','testadd15']
operatorpasswordTwo = ['testadd','testadd','testadd1','testadd2','testadd3','testadd4','testadd5',
                        'testadd6','testaddddd','','testadd9','testadd10','testadd11','testadd15']
agentNumber = ['57981030','57981030','57981031','57981032','57981033','57981034',
               '57981035','57981036','57981037','57981038','579810380','','57981039','57981040']
agentIp = ['192.168.1.123','192.168.1.123','192.168.1.124','192.168.1.125','192.168.1.126','192.168.1.127','192.168.1.128','192.168.1.129',
           '192.168.1.130', '192.168.1.131','192.168.1.132','192.168.1.133','192.','']
add_operator_info = [u'保存成功！',u'操作员名称重复，请重新输入！',u'操作员名称超长,操作员名称不能超过10个字符',u'操作员名称不能为空！',
                        u'真实姓名不能包含除“-_”外的特殊字符！',u'真实姓名不能为空！',u'登录密码超长!',u'请输入登录密码!',u'登录密码和确认密码不一致!',
                     u'请输入确认密码!',u'坐席号码长度超长',u'坐席号码不能为空！',u'请输入合法的坐席IP！',u'请输入坐席IP！']
account1 = ['testedit','']
operatorname1 = ['testedit','']
operatorpassword1 = ['testedit','']
operatorpasswordTwo1 = ['testedit','']
agentNumber1 = ['57981055','']
agentIp1 = ['192.168.1.134','']
edit_operator_info = [u'保存成功！',u'操作员名称不能为空！']

#workschedule parameter
workschedulename = [u'作息名称',u'作息名称','','TestA','TestB']
startTime = ['20190501','20190501','20190601','','20190801']
releaseTime = ['20190530','20190530','20190630','20190730','']
add_workschedule_info = [u'保存成功!',u'名称不能重复!',u'名称不允许为空!',u'生效日期不允许为空',u'截止日期不允许为空']
workschedulename1 = [u'修改后的作息名称','']
edit_workschedule_info = [u'修改成功!',u'名称不允许为空!']

#knowledgebase parameter
KBMname = ['Test','Test','','@Test']
addknowledgebase_info = [u'添加成功',u'目录名称不能重复',u'目录名称不能为空',u'目录名称不能输入非法字符，如&<>?等']
filename = ['TestA','TestA','TestB@','','TestD','TestE','TestF']
title = ['TestA','TestA','TestB','TestC','','TestE','TestF']
releaseUnit = ['TestA','TestA','TestB','TestC','TestD','','TestF']
content = ['TestA','TestA','TestB','TestC','TestD','TestE','']
addknowledgebasefile_info = [u'添加成功',u'目录名称不能重复',u'文件名称不能输入非法字符，如&<>?等',u'文件名称不能为空',u'文件标题不能为空',
                             u'发布单位不能为空',u'内容不能为空']
KBMname1 = ['TestNew','']
editknowledgebase_info = [u'修改成功',u'目录名称不能为空']

filename1 = ['TestG','']
title1 = ['TestG','']
releaseUnit1 = ['TestG','']
content1 = ['TestG','']
editknowledgebasefile_info = [u'修改成功',u'文件名称不能为空']

#report parameter
reportfilename = 'D:\Report\Report.html'





















