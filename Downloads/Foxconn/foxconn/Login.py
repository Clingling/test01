# -*- coding:UTF-8 -*-

from selenium import webdriver

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import  WebDriverWait
import unittest
import time
import data
import re






driver = data.return_driver()

class Login(unittest.TestCase):
    def setUp(self):
        pass

    def login(self, userName, password):
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,"userName"))).clear()
        #driver.find_element_by_id("userName").clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,"userName"))).send_keys(userName)
        #driver.find_element_by_id("userName").send_keys(userName)
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,"password"))).clear()
        #driver.find_element_by_id("password").clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,"password"))).send_keys(password)
        #driver.find_element_by_id("password").send_keys(password)
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,"btn"))).click()
        #driver.find_element_by_id("btn").click()
        time.sleep(5)
        alert = driver.find_element_by_class_name("number").text
        print alert
        self.assertIn("admin", alert)

    def test_login_success(self):
       self.login(data.userName,data.password)

if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login("test_login_success"))
    runner = unittest.TextTestRunner()
    runner.run(suite)