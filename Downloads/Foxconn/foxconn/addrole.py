# -*- coding: UTF-8 -*-
from selenium import webdriver
#导入 WebDriverWait 包
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import  WebDriverWait
import time

import Login
import data
import unittest


driver = data.return_driver()

class Addrole(unittest.TestCase):
    def setUp(self):
        pass

    def test_addrole(self):
        try:
            time.sleep(2)
            #进入系统管理
            driver.switch_to_default_content()
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.LINK_TEXT, "系统管理"))).click()
            # time.sleep(2)
            #进入角色管理
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.LINK_TEXT, "角色管理"))).click()
            print "successfully enter the role interface"
        except:
            print "can not enter the role interface"

    def addrole(self,rolename,roledescription):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, "btn_add"))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID,'_frame_layer')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'name'))).clear()
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'name'))).send_keys(rolename)
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'description'))).clear()
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.ID, 'description'))).send_keys(roledescription)
        time.sleep(2)
        #点击权限配置
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH,".//*[@id='role']/div[3]/span/input[2]"))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, '_frame_layer')))
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH,"html/body/div[1]/div[2]/div[1]/ul/li[1]/ul/li[1]/div/span[4]"))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,"html/body/div[1]/div[1]/input[1]"))).click()
        time.sleep(5)
        #点击保存
        driver.switch_to_default_content()
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, '_frame_layer')))
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,"html/body/div[1]/div[1]/input[1]"))).click()
        time.sleep(2)

    def test_addrole_success(self):
        self.addrole(data.rolename[0],data.roledescription[0])
        alert = WebDriverWait(driver, 10).until(lambda x:x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_role_info[0],alert)
        #关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH,"//input[starts-with(@id,'btn_')]"))).click()

    def test_addrole_repeat(self):
        self.addrole(data.rolename[1],data.roledescription[1])
        alert = WebDriverWait(driver, 10).until(lambda x:x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_role_info[1],alert)
        #关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH,"//input[starts-with(@id,'btn_')]"))).click()
        #点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH,"html/body/div/div[1]/input[2]"))).click()

    def test_addrole_name_error(self):
        self.addrole(data.rolename[2],data.roledescription[2])
        alert = WebDriverWait(driver, 10).until(lambda x:x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_role_info[2],alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div/div[1]/input[2]"))).click()

    def test_addrole_name_blank(self):
        self.addrole(data.rolename[3],data.roledescription[3])
        alert = WebDriverWait(driver, 10).until(lambda x:x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_role_info[3],alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div/div[1]/input[2]"))).click()

    def test_addrole_power_blank(self):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, "btn_add"))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, '_frame_layer')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).clear()
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).send_keys('Test3')
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'description'))).clear()
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'description'))).send_keys('Test3')
        time.sleep(2)
        #点击保存
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,"html/body/div[1]/div[1]/input[1]"))).click()
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_role_info[4], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div/div[1]/input[2]"))).click()

if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(Addrole("test_addrole"))
    suite.addTest(Addrole("test_addrole_success"))
    suite.addTest(Addrole("test_addrole_repeat"))
    suite.addTest(Addrole("test_addrole_name_error"))
    suite.addTest(Addrole("test_addrole_name_blank"))
    suite.addTest(Addrole("test_addrole_power_blank"))
    runner = unittest.TextTestRunner()
    runner.run(suite)

