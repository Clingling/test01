# -*- coding: UTF-8 -*-
from selenium import webdriver
#导入 WebDriverWait 包
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import  WebDriverWait
import time

import Login
import data
import unittest


driver = data.return_driver()

class Deleterole(unittest.TestCase):
    def setUp(self):
        pass

    def test_deleterole(self):
        try:
            time.sleep(2)
            #进入系统管理
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.LINK_TEXT, "系统管理"))).click()
            time.sleep(2)
            #进入角色管理
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.LINK_TEXT, "角色管理"))).click()
            print "successfully enter the role interface"
        except:
            print "can not enter the role interface"

    def test_deleterole_cancel(self):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, ".//*[@id='DataTable']/tbody/tr[1]/td[4]/a[2]"))).click()
        time.sleep(2)
        #点击取消
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH,"html/body/div[4]/div[1]/div[2]/div[2]/input[2]"))).click()
        print "cancel to delete role"

    def test_deleterole_success(self):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, ".//*[@id='DataTable']/tbody/tr[1]/td[4]/a[2]"))).click()
        time.sleep(2)
        #点击确定
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[4]/div[1]/div[2]/div[2]/input[1]"))).click()
        # 关闭提示框
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[4]/div[1]/div[2]/div[2]/input"))).click()
        print "successful delete role"

if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(Deleterole("test_deleterole"))
    suite.addTest(Deleterole("test_deleterole_cancel"))
    suite.addTest(Deleterole("test_deleterole_success"))
    runner = unittest.TextTestRunner()
    runner.run(suite)