# -*- coding: UTF-8 -*-
from selenium import webdriver
#导入 WebDriverWait 包
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import  WebDriverWait
from selenium.webdriver.support.ui import Select
import time

import Login
import data
import unittest


driver = data.return_driver()

class Editknowledgebase(unittest.TestCase):
    def setUp(self):
        pass

    def test_editknowledgebase(self):
        try:
            time.sleep(2)
            #进入知识库管理
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.LINK_TEXT, "知识库管理"))).click()
            print "successfully enter the knowledgebase interface"
        except:
            print "can not enter the knowledgebase interface"

    def editknowledgebase(self,KBMname1):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'leftFrame')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div/div/div/ul/li/ul/li[1]/div/span[4]/span"))).click()
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'rightFrame')))
        time.sleep(2)
        #点击编辑目录
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'modifyUnit'))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'name'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'name'))).send_keys(KBMname1)
        # 点击保存
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'savebtn'))).click()

    def editknowledgebasefile(self,filename1,title1,releaseUnit1,content1):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'leftFrame')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div/div/div/ul/li/ul/li[1]/ul/li/div/span[5]/span"))).click()
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'rightFrame')))
        time.sleep(2)
        # 点击修改
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,".//*[@id='DataTable']/tbody/tr/td[9]/a[2]"))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).send_keys(filename1)
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'title'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'title'))).send_keys(title1)
        time.sleep(2)
        Select(driver.find_element_by_id("fileLevel")).select_by_visible_text(u"二级")
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'releaseUnit'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'releaseUnit'))).send_keys(releaseUnit1)
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'content'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'content'))).send_keys(content1)
        # 点击保存
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'savebtn'))).click()

    def test_editknowledgebase_success(self):
        self.editknowledgebase(data.KBMname1[0])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name('zxui-prompt-inf')).text
        print alert
        self.assertIn(data.editknowledgebase_info[0], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()

    def test_editknowledgebase_blank(self):
        self.editknowledgebase(data.KBMname1[1])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name('zxui-prompt-inf')).text
        print alert
        self.assertIn(data.editknowledgebase_info[1], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

    def test_editknowledgebasefile_success(self):
        self.editknowledgebasefile(data.filename1[0], data.title1[0], data.releaseUnit1[0], data.content1[0])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name('zxui-prompt-inf')).text
        print alert
        self.assertIn(data.editknowledgebasefile_info[0], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()

    def test_editknowledgebasefile_blank(self):
        self.editknowledgebasefile(data.filename1[1], data.title1[1], data.releaseUnit1[1], data.content1[1])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name('zxui-prompt-inf')).text
        print alert
        self.assertIn(data.editknowledgebasefile_info[1], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(Editknowledgebase("test_editknowledgebase"))
    suite.addTest(Editknowledgebase("test_editknowledgebase_success"))
    suite.addTest(Editknowledgebase("test_editknowledgebase_blank"))
    suite.addTest(Editknowledgebase("test_editknowledgebasefile_success"))
    suite.addTest(Editknowledgebase("test_editknowledgebasefile_blank"))
    runner = unittest.TextTestRunner()
    runner.run(suite)