# -*- coding: UTF-8 -*-
from selenium import webdriver
#导入 WebDriverWait 包
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import  WebDriverWait
import time

import Login
import data
import unittest


driver = data.return_driver()

class Editworkschedule(unittest.TestCase):
    def setUp(self):
        pass

    def test_editworkschedule(self):
        try:
            time.sleep(2)
            #进入系统管理
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.LINK_TEXT, "系统管理"))).click()
            time.sleep(2)
            #进入作息管理
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.LINK_TEXT, "作息管理"))).click()
            print "successfully enter the workschedule interface"
        except:
            print "can not enter the workschedule interface"

    def editworkschedule(self,workschedulename1):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        time.sleep(2)
        #点击添加
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, ".//*[@id='DataTable']/tbody/tr[1]/td[7]/a[2]"))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, '_frame_layer')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'name'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'name'))).send_keys(workschedulename1)
        time.sleep(2)
        '''#选择起止日期
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'startTime'))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'startTime'))).send_keys(startTime1)
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'releaseTime'))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'releaseTime'))).send_keys(releaseTime1)
        time.sleep(2)'''
        #点击保存
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,"html/body/div[1]/div[2]/input[1]"))).click()

    def test_editworkschedule_success(self):
        self.editworkschedule(data.workschedulename1[0])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.edit_workschedule_info[0], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()

    def test_editworkschedule_blank(self):
        self.editworkschedule(data.workschedulename1[1])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.edit_workschedule_info[1], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[2]/input[2]"))).click()

if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(Editworkschedule("test_editworkschedule"))
    suite.addTest(Editworkschedule("test_editworkschedule_success"))
    suite.addTest(Editworkschedule("test_editworkschedule_blank"))
    runner = unittest.TextTestRunner()
    runner.run(suite)