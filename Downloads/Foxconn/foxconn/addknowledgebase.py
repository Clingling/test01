# -*- coding: UTF-8 -*-
from selenium import webdriver
#导入 WebDriverWait 包
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import  WebDriverWait
from selenium.webdriver.support.ui import Select
import time
import Login
import data
import unittest


driver = data.return_driver()

class Addknowledgebase(unittest.TestCase):
    def setUp(self):
        pass

    def test_addknowledgebase(self):
        try:
            time.sleep(2)
            #进入知识库管理
            driver.switch_to_default_content()
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.LINK_TEXT, "知识库管理"))).click()
            print "successfully enter the knowledgebase interface"
        except:
            print "can not enter the knowledgebase interface"

    #添加目录
    def addknowledgebase(self,KBMname):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID,'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID,'rightFrame')))
        time.sleep(2)
        #添加目录
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,"addUnit"))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID,"_frame_layer")))
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'name'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'name'))).send_keys(KBMname)
        time.sleep(2)
        #点击保存
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'savebtn'))).click()

    #添加文件
    def addknowledgebasefile(self,filename,title,releaseUnit,content):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID,'main')))
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID,'leftFrame')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,"html/body/div/div/div/ul/li/ul/li[1]/div/span[4]/span"))).click()
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID,'rightFrame')))
        time.sleep(2)
        #点击添加文件
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'addFile'))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID,"_frame_layer")))
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'name'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'name'))).send_keys(filename)
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'title'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'title'))).send_keys(title)
        time.sleep(2)
        Select(driver.find_element_by_id("fileLevel")).select_by_visible_text(u"一级")
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'releaseUnit'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'releaseUnit'))).send_keys(releaseUnit)
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'content'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'content'))).send_keys(content)
        #点击保存
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'savebtn'))).click()

    def test_addknowledgebase_success(self):
        self.addknowledgebase(data.KBMname[0])
        alert = WebDriverWait(driver, 10).until(lambda x:x.find_element_by_class_name('zxui-prompt-inf')).text
        print alert
        self.assertIn(data.addknowledgebase_info[0],alert)
        #关闭提示框
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,"//input[starts-with(@id,'btn_')]"))).click()

    def test_addknowledgebase_repeat(self):
        self.addknowledgebase(data.KBMname[1])
        alert = WebDriverWait(driver, 10).until(lambda x:x.find_element_by_class_name('zxui-prompt-inf')).text
        print alert
        self.assertIn(data.addknowledgebase_info[1],alert)
        #关闭提示框
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,"//input[starts-with(@id,'btn_')]"))).click()
        #点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,"html/body/div[1]/div[1]/input[2]"))).click()

    def test_addknowledgebase_blank(self):
        self.addknowledgebase(data.KBMname[2])
        alert = WebDriverWait(driver, 10).until(lambda x:x.find_element_by_class_name('zxui-prompt-inf')).text
        print alert
        self.assertIn(data.addknowledgebase_info[2],alert)
        #关闭提示框
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,"//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

    def test_addknowledgebase_error(self):
        self.addknowledgebase(data.KBMname[3])
        alert = WebDriverWait(driver, 10).until(lambda x:x.find_element_by_class_name('zxui-prompt-inf')).text
        print alert
        self.assertIn(data.addknowledgebase_info[3],alert)
        #关闭提示框
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,"//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

    def test_addknowledgebasefile_success(self):
        self.addknowledgebasefile(data.filename[0],data.title[0],data.releaseUnit[0],data.content[0])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name('zxui-prompt-inf')).text
        print alert
        self.assertIn(data.addknowledgebasefile_info[0], alert)
        # 关闭提示框
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()

    def test_addknowledgebasefile_repeat(self):
        self.addknowledgebasefile(data.filename[1], data.title[1], data.releaseUnit[1], data.content[1])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name('zxui-prompt-inf')).text
        print alert
        self.assertIn(data.addknowledgebasefile_info[1], alert)
        # 关闭提示框
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

    def test_addknowledgebasefile_name_error(self):
        self.addknowledgebasefile(data.filename[2], data.title[2], data.releaseUnit[2], data.content[2])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name('zxui-prompt-inf')).text
        print alert
        self.assertIn(data.addknowledgebasefile_info[2], alert)
        # 关闭提示框
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

    def test_addknowledgebasefile_name_blank(self):
        self.addknowledgebasefile(data.filename[3], data.title[3], data.releaseUnit[3], data.content[3])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name('zxui-prompt-inf')).text
        print alert
        self.assertIn(data.addknowledgebasefile_info[3], alert)
        # 关闭提示框
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

    def test_addknowledgebasefile_title_blank(self):
        self.addknowledgebasefile(data.filename[4], data.title[4], data.releaseUnit[4], data.content[4])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name('zxui-prompt-inf')).text
        print alert
        self.assertIn(data.addknowledgebasefile_info[4], alert)
        # 关闭提示框
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

    def test_addknowledgebasefile_releaseUnit_blank(self):
        self.addknowledgebasefile(data.filename[5], data.title[5], data.releaseUnit[5], data.content[5])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name('zxui-prompt-inf')).text
        print alert
        self.assertIn(data.addknowledgebasefile_info[5], alert)
        # 关闭提示框
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

    def test_addknowledgebasefile_content_blank(self):
        self.addknowledgebasefile(data.filename[6], data.title[6], data.releaseUnit[6], data.content[6])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name('zxui-prompt-inf')).text
        print alert
        self.assertIn(data.addknowledgebasefile_info[6], alert)
        # 关闭提示框
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(Addknowledgebase("test_addknowledgebase"))
    suite.addTest(Addknowledgebase("test_addknowledgebase_success"))
    suite.addTest(Addknowledgebase("test_addknowledgebase_repeat"))
    suite.addTest(Addknowledgebase("test_addknowledgebase_blank"))
    suite.addTest(Addknowledgebase("test_addknowledgebase_error"))
    suite.addTest(Addknowledgebase("test_addknowledgebasefile_success"))
    suite.addTest(Addknowledgebase("test_addknowledgebasefile_repeat"))
    suite.addTest(Addknowledgebase("test_addknowledgebasefile_name_error"))
    suite.addTest(Addknowledgebase("test_addknowledgebasefile_name_blank"))
    suite.addTest(Addknowledgebase("test_addknowledgebasefile_title_blank"))
    suite.addTest(Addknowledgebase("test_addknowledgebasefile_releaseUnit_blank"))
    suite.addTest(Addknowledgebase("test_addknowledgebasefile_content_blank"))
    runner = unittest.TextTestRunner()
    runner.run(suite)