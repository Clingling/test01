# -*- coding:UTF-8 -*-

from selenium import webdriver

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
import unittest
import Login
import time
import data
import re

driver = data.return_driver()

class Business(unittest.TestCase):
    def setUp(self):
        pass

    def test_Business_manage(self):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        #点击业务类型管理
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.LINK_TEXT, "业务类型管理"))).click()
        print "Enter the Business interface"



    def add_L_Business(self,L_name):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "rightFrame")))
        #点击添加业务大类
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[1]/input[1]'))).click()
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))

        time.sleep(3)
        #输入类型名
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).send_keys(L_name)
        #点击保存
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'savebtn'))).click()




    #添加大类成功检查
    def test_add_L_Business_success(self):
        self.add_L_Business(data.L_name[0])
        #检查保存成功提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.business_info[0], alert)
        time.sleep(10)
        # 关闭提示框
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()


  #添大类名称重复检查
    def test_add_L_Business_repeat(self):
        self.add_L_Business(data.L_name[0])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.business_info[1], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        #点击取消
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()


        # 添大类名称为空检查
    def test_add_L_Business_empty(self):
        self.add_L_Business(data.L_name[1])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.business_info[3], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()

    #修改大类检查
    def edit_L_Business(self, L_name):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        #切换左边的iframe
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "leftFrame")))
        time.sleep(1)
        #点击大类名称
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/div/ul/li[1]/div/span[3]/span'))).click()
        #切换右边的iframe
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "rightFrame")))
      # 点击修改

        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'updateFolder'))).click()
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        time.sleep(3)
        # 清除大类名称
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).clear()
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).send_keys(L_name)
        # 点击保存
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'savebtn'))).click()


    #修改大类成功检查
    def test_edit_L_Business_success(self):
        self.edit_L_Business(data.L_name[2])
        # 检查保存成功提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.business_info[2], alert)
        time.sleep(10)
        # 关闭提示框
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()

        # 修改大类名称重复检查
    def test_edit_L_Business_repeat(self):
         self.edit_L_Business(data.L_name[3])
         alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
         print alert
         self.assertIn(data.business_info[1], alert)
         # 关闭提示框
         time.sleep(10)
         # 点击确定
         WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
         # 点击取消
         time.sleep(1)
         WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()

    # 修改大类名称为空检查
    def test_edit_L_Business_empty(self):
         self.edit_L_Business(data.L_name[1])
         alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
         print alert
         self.assertIn(data.business_info[3], alert)
         # 关闭提示框
         time.sleep(10)
         # 点击确定
         WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
         # 点击取消
         time.sleep(1)
         WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()




     #新增小类
    def add_S_Business(self, S_name):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        # 切换左边的iframe
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "leftFrame")))
        time.sleep(1)
        # 点击大类名称
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/div/ul/li[1]/div/span[3]/span'))).click()

        # 切换右边的iframe
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "rightFrame")))

        # 点击新增小类
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'addFile'))).click()
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).send_keys(S_name)
        # 点击保存
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'savebtn'))).click()



        # 添加小类成功检查
    def test_add_S_Business_success(self):
        self.add_S_Business(data.S_name[0])
        # 检查保存成功提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.business_info[0], alert)
        time.sleep(10)
        # 关闭提示框
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()

        # 添加小类成功检查
    def test_add_S_Business_success2(self):
        self.add_S_Business(data.S_name[3])
        # 检查保存成功提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.business_info[0], alert)
        time.sleep(10)
        # 关闭提示框
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()

        # 添小类名称重复检查
    def test_add_S_Business_repeat(self):
        self.add_S_Business(data.S_name[0])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.business_info[1], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()


        # 添小类名称为空检查
    def test_add_S_Business_empty(self):
        self.add_S_Business(data.S_name[1])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.business_info[3], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()

    def edit_S_Business(self, S_name):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        # 切换左边的iframe
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "leftFrame")))
        time.sleep(1)
        # 点击大类名称
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/div/ul/li[1]/div/span[3]/span'))).click()

        # 切换右边的iframe
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "rightFrame")))

        # 点击修改
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[2]/table/tbody/tr/td[5]/a[1]'))).click()
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        time.sleep(1)

        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).clear()
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).send_keys(S_name)
        # 点击保存
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'savebtn'))).click()





        # 修改小类成功检查
    def test_edit_S_Business_success(self):
        self.edit_S_Business(data.S_name[2])
        # 检查保存成功提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.business_info[2], alert)
        time.sleep(10)
        # 关闭提示框
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()

            # 修改大类名称重复检查

    def test_edit_S_Business_repeat(self):
        self.edit_S_Business(data.S_name[3])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.business_info[1], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()

        # 修改大类名称为空检查
    def test_edit_S_Business_empty(self):
        self.edit_S_Business(data.S_name[1])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.business_info[3], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()



    #删除小类
    def test_delete_S_Business(self):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        # 切换左边的iframe
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "leftFrame")))
        time.sleep(1)
        # 点击大类名称
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/div/ul/li[1]/div/span[3]/span'))).click()

        # 切换右边的iframe
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "rightFrame")))

        #点击删除
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[2]/table/tbody/tr[1]/td[5]/a[2]'))).click()
        #点击确定
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[3]/div/div[2]/div[2]/input[1]'))).click()
        time.sleep(1)
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.business_info[4], alert)
        time.sleep(10)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[3]/div/div[2]/div[2]/input'))).click()



    #删除大类
    def test_delete_L_Business(self):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        # 切换左边的iframe
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "leftFrame")))
        time.sleep(1)
        # 点击大类名称
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/div/ul/li[1]/div/span[3]/span'))).click()

        # 切换右边的iframe
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "rightFrame")))

        #点击删除
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'deleteFolder'))).click()
        #点击确定
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[3]/div/div[2]/div[2]/input[1]'))).click()
        time.sleep(1)
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.business_info[4], alert)
        time.sleep(10)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[3]/div/div[2]/div[2]/input'))).click()





if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(Business("test_Business_manage"))
    #添加大类
    # suite.addTest(Business("test_add_L_Business_success"))
    # suite.addTest(Business("test_add_L_Business_repeat"))
    # suite.addTest(Business("test_add_L_Business_empty"))
    # #修改大类
    # suite.addTest(Business("test_edit_L_Business_success"))
    # suite.addTest(Business("test_edit_L_Business_repeat"))
    # suite.addTest(Business("test_edit_L_Business_empty"))
    #添加小类
    suite.addTest(Business("test_add_S_Business_success"))
    suite.addTest(Business("test_add_S_Business_success2"))
    suite.addTest(Business("test_add_S_Business_repeat"))
    suite.addTest(Business("test_add_S_Business_empty"))
    #修改小类
    suite.addTest(Business("test_edit_S_Business_success"))
    suite.addTest(Business("test_edit_S_Business_repeat"))
    suite.addTest(Business("test_edit_S_Business_empty"))


    #  #删除小类
    suite.addTest(Business("test_delete_S_Business"))
        #删除大类
    suite.addTest(Business("test_delete_L_Business"))
    runner = unittest.TextTestRunner()
    runner.run(suite)