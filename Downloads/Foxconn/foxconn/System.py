# -*- coding:UTF-8 -*-

from selenium import webdriver

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
import unittest
import Login
import time
import data
import re

driver = data.return_driver()

class System(unittest.TestCase):
    def setUp(self):
        pass

    def test_System_manage(self):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        #点击系统配置
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.LINK_TEXT, "配置管理"))).click()

        print "Enter the Syatem management interface"



    def add_System(self,itemName,itemValue,itemNameCn):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        #点击添加
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'btn_add'))).click()
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        time.sleep(3)
        #输入配置项名称
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'itemName'))).send_keys(itemName)
        #输入配置项值
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'itemValue'))).send_keys(itemValue)
        #输入描述
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'itemNameCn'))).send_keys(itemNameCn)
       #点击保存
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[1]'))).click()



    #添加成功检查
    def test_add_System_success(self):
        self.add_System(data.itemName[0],data.itemValue[0] , data.itemNameCn[0])
        #检查保存成功提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.system_info[0], alert)
        time.sleep(10)
        # 关闭提示框
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()


  #添加配置项名称重复检查
    def test_add_itemName_repeat(self):
        self.add_System(data.itemName[0], data.itemValue[0], data.itemNameCn[0])
        #检查配置项名称重复提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.system_info[3], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        #点击取消
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()



  #添加配置项名称为空检查
    def test_add_itemName_empty(self):
        self.add_System(data.itemName[2], data.itemValue[0], data.itemNameCn[0])

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.system_info[2], alert)
        # 关闭提示框
        time.sleep(10)
        #点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        #点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()


        # 添加配置项名称过长检查
    def test_add_itemName_length(self):
        self.add_System(data.itemName[3], data.itemValue[0], data.itemNameCn[0])

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.system_info[1], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()




        # 添加配置项值为空检查
    def test_add_itemValue_empty(self):
        self.add_System(data.itemName[1], data.itemValue[1], data.itemNameCn[0])

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.system_info[6], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        time.sleep(5)
        # 点击取消
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()




        # 添加配置项值过长检查
    def test_add_itemValue_length(self):
        self.add_System(data.itemName[1], data.itemValue[3], data.itemNameCn[0])

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.system_info[5], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        time.sleep(5)
        # 点击取消
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()


        # 添加配置项值特殊字符检查
    def test_add_itemValue_format(self):
        self.add_System(data.itemName[1], data.itemValue[2], data.itemNameCn[0])

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.system_info[4], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()





        # 添加配置项描述为空检查
    def test_add_itemNameCn_empty(self):
        self.add_System(data.itemName[1], data.itemValue[0], data.itemNameCn[2])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.system_info[8], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()



        # 添加配置项描述过长检查
    def test_add_itemNameCn_length(self):
        self.add_System(data.itemName[1], data.itemValue[0], data.itemNameCn[3])

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.system_info[7], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until( EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()









  #修改配置项
    def edit_System(self,itemName,itemValue,itemNameCn):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        #清空查询条件
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[1]/ul/li[1]/span[2]/span/input'))).clear()
        #输入查询条件
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[1]/ul/li[1]/span[2]/span/input'))).send_keys(u'配置项')
        #点击查询
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[1]/ul/li[2]/span[1]/input'))).click()
        #点击修改
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[2]/table/tbody/tr/td[5]/a[1]'))).click()
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "_frame_layer")))
        time.sleep(3)
        #清除标题内容
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'itemName'))).clear()
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'itemName'))).send_keys(itemName)
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'itemValue'))).clear()
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'itemValue'))).send_keys(itemValue)
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'itemNameCn'))).clear()
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'itemNameCn'))).send_keys(itemNameCn)
       #点击保存
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[1]'))).click()



        # 修改成功检查
    def test_edit_System_success(self):
        self.edit_System(data.itemName[1], data.itemValue[0], data.itemNameCn[0])
        # 检查保存成功提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.system_info[0], alert)
        time.sleep(10)
        # 关闭提示框
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()



        # 修改配置项名称重复检查
    def test_edit_itemName_repeat(self):
        self.edit_System(data.itemName[4], data.itemValue[0], data.itemNameCn[1])
        # 检查配置项名称重复提示语
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.system_info[3], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()


        # 修改配置项名称为空检查
    def test_edit_itemName_empty(self):
        self.edit_System(data.itemName[2], data.itemValue[0], data.itemNameCn[0])

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.system_info[2], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()



        # 修改配置项名称过长检查
    def test_edit_itemName_length(self):
        self.edit_System(data.itemName[3], data.itemValue[0], data.itemNameCn[0])

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.system_info[1], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()



        # 修改配置项值为空检查
    def test_edit_itemValue_empty(self):
        self.edit_System(data.itemName[0], data.itemValue[1], data.itemNameCn[0])

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.system_info[6], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        time.sleep(5)
        # 点击取消
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()



        # 修改配置项值过长检查
    def test_edit_itemValue_length(self):
        self.edit_System(data.itemName[0], data.itemValue[3], data.itemNameCn[0])

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.system_info[5], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        time.sleep(5)
        # 点击取消
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()



        # 修改配置项值特殊字符检查
    def test_edit_itemValue_format(self):
        self.edit_System(data.itemName[0], data.itemValue[2], data.itemNameCn[0])

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.system_info[4], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()



        # 修改配置项描述为空检查
    def test_edit_itemNameCn_empty(self):
        self.edit_System(data.itemName[0], data.itemValue[0], data.itemNameCn[2])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.system_info[8], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()



        # 修改配置项描述过长检查
    def test_edit_itemNameCn_length(self):
        self.edit_System(data.itemName[0], data.itemValue[0], data.itemNameCn[3])

        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.system_info[7], alert)
        # 关闭提示框
        time.sleep(10)
        # 点击确定
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/input'))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/input[2]'))).click()







  #删除配置项
    def test_delete_System(self):
        time.sleep(3)
        driver.switch_to_default_content()
        time.sleep(3)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        #清空查询条件
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[1]/ul/li[1]/span[2]/span/input'))).clear()
        #输入查询条件
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[1]/ul/li[1]/span[2]/span/input'))).send_keys(u'配置项')
        #点击查询
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[1]/ul/li[2]/span[1]/input'))).click()
        #点击删除
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[2]/table/tbody/tr/td[5]/a[2]'))).click()
        #点击确定
        time.sleep(1)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[4]/div/div[2]/div[2]/input[1]'))).click()
        time.sleep(1)
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.system_info[9], alert)
        time.sleep(10)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[4]/div/div[2]/div[2]/input'))).click()



if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(System("test_System_manage"))
    #添加
    suite.addTest(System("test_add_System_success"))
    suite.addTest(System("test_add_itemName_repeat"))
    suite.addTest(System("test_add_itemName_empty"))
    suite.addTest(System("test_add_itemName_length"))
    suite.addTest(System("test_add_itemValue_empty"))
    suite.addTest(System("test_add_itemValue_length"))
    suite.addTest(System("test_add_itemValue_format"))
    suite.addTest(System("test_add_itemNameCn_empty"))
    suite.addTest(System("test_add_itemNameCn_length"))
   #修改
    suite.addTest(System("test_edit_System_success"))
    suite.addTest(System("test_edit_itemName_repeat"))
    suite.addTest(System("test_edit_itemName_empty"))
    suite.addTest(System("test_edit_itemName_length"))
    suite.addTest(System("test_edit_itemValue_empty"))
    suite.addTest(System("test_edit_itemValue_length"))
    suite.addTest(System("test_edit_itemValue_format"))
    suite.addTest(System("test_edit_itemNameCn_empty"))
    suite.addTest(System("test_edit_itemNameCn_length"))
    #删除
    suite.addTest(System("test_delete_System"))
    runner = unittest.TextTestRunner()
    runner.run(suite)