# -*- coding: UTF-8 -*-
from selenium import webdriver
#导入 WebDriverWait 包
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import  WebDriverWait
import time

import Login
import data
import unittest


driver = data.return_driver()

class Editlocation(unittest.TestCase):
    def setUp(self):
        pass

    def test_editlocation(self):
        try:
            time.sleep(2)
            #
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.LINK_TEXT, "归属地管理"))).click()
            print "successfully enter the l归属地管理ocation interface"
        except:
            print "can not enter the location interface"

    def editLocation(self,area1):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH,".//*[@id='DataTable']/tbody/tr[1]/td[4]/a[1]"))).click()
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, '_frame_layer')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, "area"))).clear()
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, "area"))).send_keys(area1)
        #点击保存
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[2]/input[1]"))).click()
        time.sleep(2)

    def test_editLocation_success(self):
        self.editLocation(data.area1[0])
        alert = WebDriverWait(driver, 10).until(lambda x:x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.edit_location_info[0],alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()

    def test_editLocation_blank(self):
        self.editLocation(data.area1[1])
        alert = WebDriverWait(driver, 10).until(lambda x:x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.edit_location_info[1], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        #点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[2]/input[2]"))).click()

if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(Editlocation("test_editlocation"))
    suite.addTest(Editlocation("test_editLocation_success"))
    suite.addTest(Editlocation("test_editLocation_blank"))
    runner = unittest.TextTestRunner()
    runner.run(suite)
