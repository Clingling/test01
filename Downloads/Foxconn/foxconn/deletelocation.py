# -*- coding: UTF-8 -*-
from selenium import webdriver
#导入 WebDriverWait 包
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import  WebDriverWait
import time

import Login
import data
import unittest


driver = data.return_driver()

class Deletelocation(unittest.TestCase):
    def setUp(self):
        pass

    def test_deletelocation(self):
        try:
            time.sleep(2)
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.LINK_TEXT, "归属地管理"))).click()
            print "successfully enter the location interface"
        except:
            print "can not enter the location interface"

    def test_deletelocation_cancel(self):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH,".//*[@id='DataTable']/tbody/tr[1]/td[4]/a[2]"))).click()
        time.sleep(5)
        #点击取消
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH,"/html/body/div[3]/div[1]/div[2]/div[2]/input[2]"))).click()
        print "cancel to delete location"

    def test_deletelocation_success(self):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID,'main')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH,".//*[@id='DataTable']/tbody/tr[1]/td[4]/a[2]"))).click()
        time.sleep(5)
        #点击确定
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH,"/html/body/div[3]/div[1]/div[2]/div[2]/input[1]"))).click()
        time.sleep(5)
        #关闭提示框
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH,"//input[starts-with(@id,'btn_')]"))).click()
        print "successful delete location"

if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(Deletelocation("test_deletelocation"))
    suite.addTest(Deletelocation("test_deletelocation_cancel"))
    suite.addTest(Deletelocation("test_deletelocation_success"))
    runner = unittest.TextTestRunner()
    runner.run(suite)

