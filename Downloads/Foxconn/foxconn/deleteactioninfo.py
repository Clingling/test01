# -*- coding: UTF-8 -*-
from selenium import webdriver
#导入 WebDriverWait 包
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import  WebDriverWait
import time

import Login
import data
import unittest


driver = data.return_driver()

class Deleteactioninfo(unittest.TestCase):
    def setUp(self):
        pass

    def test_deleteactioninfo(self):
        try:
            time.sleep(2)
            #进入系统管理
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.LINK_TEXT, "系统管理"))).click()
            time.sleep(2)
            #进入权限项管理
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.LINK_TEXT, "权限项管理"))).click()
            print "successfully enter the location interface"
        except:
            print "can not enter the location interface"

    def test_deleteactioninfo_cancel(self):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, ".//*[@id='DataTable']/tbody/tr[1]/td[5]/a[2]"))).click()
        time.sleep(5)
        # 点击取消
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "/html/body/div[4]/div[1]/div[2]/div[2]/input[2]"))).click()
        print "cancel to delete actioninfo"

    def test_deleteactioninfo_success(self):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, ".//*[@id='DataTable']/tbody/tr[1]/td[5]/a[2]"))).click()
        time.sleep(5)
        # 点击确定
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "/html/body/div[4]/div[1]/div[2]/div[2]/input[1]"))).click()
        time.sleep(10)
        # 关闭提示框
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "/html/body/div[4]/div[1]/div[2]/div[2]/input"))).click()
        print "successful delete actioninfo"

if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(Deleteactioninfo("test_deleteactioninfo"))
    suite.addTest(Deleteactioninfo("test_deleteactioninfo_cancel"))
    suite.addTest(Deleteactioninfo("test_deleteactioninfo_success"))
    runner = unittest.TextTestRunner()
    runner.run(suite)