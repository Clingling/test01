# -*- coding: UTF-8 -*-
from selenium import webdriver
#导入 WebDriverWait 包
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import  WebDriverWait
from selenium.webdriver.support.ui import Select
import time

import Login
import data
import unittest


driver = data.return_driver()

class Addoperator(unittest.TestCase):
    def setUp(self):
        pass

    def test_addoperator(self):
        try:
            time.sleep(2)
            # 进入系统管理
            driver.switch_to_default_content()
            # WebDriverWait(driver, 10).until(
            #     EC.element_to_be_clickable((By.XPATH, ".//*[@id='a0013']/a/samp"))).click()
            # time.sleep(2)
            # 进入操作员管理
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.LINK_TEXT, "操作员管理"))).click()
            print "successfully enter the operator interface"
        except:
            print "can not enter the operator interface"

    def addoperator(self,account,operatorname,operatorpassword,operatorpasswordTwo,agentNumber,agentIp):
        time.sleep(2)
        driver.switch_to_default_content()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'main')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, "btn_add"))).click()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, '_frame_layer')))
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'account'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID,'account'))).send_keys(account)
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'name'))).send_keys(operatorname)
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'password'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'password'))).send_keys(operatorpassword)
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'passwordTwo'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'passwordTwo'))).send_keys(operatorpasswordTwo)
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'agentNumber'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'agentNumber'))).send_keys(agentNumber)
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'agentIp'))).clear()
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'agentIp'))).send_keys(agentIp)
        time.sleep(2)
        Select(driver.find_element_by_id("roles")).select_by_visible_text(u"外呼任务话务员")
        time.sleep(2)
        Select(driver.find_element_by_id("callOut")).select_by_visible_text(u"是")
        time.sleep(2)
        Select(driver.find_element_by_id("agentType")).select_by_visible_text(u"话务员")
        time.sleep(2)
        Select(driver.find_element_by_id("parentId")).select_by_visible_text(u"接听主管")
        time.sleep(2)
        Select(driver.find_element_by_id("trunk")).select_by_visible_text(u"龙华中继")
        time.sleep(2)
        Select(driver.find_element_by_id("agentNet")).select_by_visible_text(u"否")
        time.sleep(2)
        Select(driver.find_element_by_id("outAgent")).select_by_visible_text(u"是")
        time.sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,"html/body/div[1]/div[1]/input[1]"))).click()

    def test_addoperator_success(self):
        self.addoperator(data.account[0],data.operatorname[0],data.operatorpassword[0],data.operatorpasswordTwo[0],
                         data.agentNumber[0],data.agentIp[0])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_operator_info[0], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()

    def test_addoperator_repeat(self):
        self.addoperator(data.account[1], data.operatorname[1], data.operatorpassword[1], data.operatorpasswordTwo[1],
                         data.agentNumber[1], data.agentIp[1])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_operator_info[1], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

    def test_addoperator_account_error(self):
        self.addoperator(data.account[2], data.operatorname[2], data.operatorpassword[2], data.operatorpasswordTwo[2],
                         data.agentNumber[2], data.agentIp[2])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_operator_info[2], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

    def test_addoperator_account_blank(self):
        self.addoperator(data.account[3], data.operatorname[3], data.operatorpassword[3], data.operatorpasswordTwo[3],
                         data.agentNumber[3], data.agentIp[3])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_operator_info[3], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

    def test_addoperator_operatorname_error(self):
        self.addoperator(data.account[4], data.operatorname[4], data.operatorpassword[4], data.operatorpasswordTwo[4],
                         data.agentNumber[4], data.agentIp[4])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_operator_info[4], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

    def test_addoperator_operatorname_blank(self):
        self.addoperator(data.account[5], data.operatorname[5], data.operatorpassword[5], data.operatorpasswordTwo[5],
                         data.agentNumber[5], data.agentIp[5])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_operator_info[5], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[2]/div[1]/div[2]/div[2]/input"))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

    def test_addoperator_operatorpassword_error(self):
        self.addoperator(data.account[6], data.operatorname[6], data.operatorpassword[6], data.operatorpasswordTwo[6],
                         data.agentNumber[6], data.agentIp[6])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_operator_info[6], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

    def test_addoperator_operatorpassword_blank(self):
        self.addoperator(data.account[7], data.operatorname[7], data.operatorpassword[7], data.operatorpasswordTwo[7],
                         data.agentNumber[7], data.agentIp[7])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_operator_info[7], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

    def test_addoperator_operatorpasswordTwo_error(self):
        self.addoperator(data.account[8], data.operatorname[8], data.operatorpassword[8], data.operatorpasswordTwo[8],
                         data.agentNumber[8], data.agentIp[8])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_operator_info[8], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

    def test_addoperator_operatorpasswordTwo_blank(self):
        self.addoperator(data.account[9], data.operatorname[9], data.operatorpassword[9], data.operatorpasswordTwo[9],
                         data.agentNumber[9], data.agentIp[9])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_operator_info[9], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

    def test_addoperator_agentNumber_error(self):
        self.addoperator(data.account[10], data.operatorname[10], data.operatorpassword[10], data.operatorpasswordTwo[10],
                         data.agentNumber[10], data.agentIp[10])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_operator_info[10], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

    def test_addoperator_agentNumber_blank(self):
        self.addoperator(data.account[11], data.operatorname[11], data.operatorpassword[11], data.operatorpasswordTwo[11],
                         data.agentNumber[11], data.agentIp[11])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_operator_info[11], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

    def test_addoperator_agentIp_error(self):
        self.addoperator(data.account[12], data.operatorname[12], data.operatorpassword[12], data.operatorpasswordTwo[12],
                         data.agentNumber[12], data.agentIp[12])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_operator_info[12], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

    def test_addoperator_agentIp_blank(self):
        self.addoperator(data.account[13], data.operatorname[13], data.operatorpassword[13], data.operatorpasswordTwo[13],
                         data.agentNumber[13], data.agentIp[13])
        alert = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_class_name("zxui-prompt-inf")).text
        print alert
        self.assertIn(data.add_operator_info[13], alert)
        # 关闭提示框
        time.sleep(10)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[starts-with(@id,'btn_')]"))).click()
        # 点击取消
        time.sleep(5)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "html/body/div[1]/div[1]/input[2]"))).click()

if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Login.Login("test_login_success"))
    suite.addTest(Addoperator("test_addoperator"))
    suite.addTest(Addoperator("test_addoperator_success"))
    suite.addTest(Addoperator("test_addoperator_repeat"))
    suite.addTest(Addoperator("test_addoperator_account_error"))
    suite.addTest(Addoperator("test_addoperator_account_blank"))
    suite.addTest(Addoperator("test_addoperator_operatorname_error"))
    suite.addTest(Addoperator("test_addoperator_operatorname_blank"))
    suite.addTest(Addoperator("test_addoperator_operatorpassword_error"))
    suite.addTest(Addoperator("test_addoperator_operatorpassword_blank"))
    suite.addTest(Addoperator("test_addoperator_operatorpasswordTwo_error"))
    suite.addTest(Addoperator("test_addoperator_operatorpasswordTwo_blank"))
    suite.addTest(Addoperator("test_addoperator_agentNumber_error"))
    suite.addTest(Addoperator("test_addoperator_agentNumber_blank"))
    suite.addTest(Addoperator("test_addoperator_agentIp_error"))
    suite.addTest(Addoperator("test_addoperator_agentIp_blank"))
    runner = unittest.TextTestRunner()
    runner.run(suite)